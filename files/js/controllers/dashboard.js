var dashboard = {
 setGrafik: function () {
  console.log('set grafik');
  
  FusionCharts.ready(function () {
   var revenueChart = new FusionCharts({
    type: 'column2d',
    renderAt: 'chart-container',
    width: '800',
    height: '400',
    dataFormat: 'json',
    dataSource: {
     "chart": {
      "caption": "Grafik Peminjaman Buku Perpustakaan",
      "subCaption": "SMK Negeri 2 Blitar",
      "xAxisName": "Bulan",
      "yAxisName": "Jumlah Siswa Pinjam Buku",
      "paletteColors": "#0075c2",
      "bgColor": "#ffffff",
      "borderAlpha": "20",
      "canvasBorderAlpha": "0",
      "usePlotGradientColor": "0",
      "plotBorderAlpha": "10",
      "placevaluesInside": "1",
      "rotatevalues": "1",
      "valueFontColor": "#ffffff",
      "showXAxisLine": "1",
      "xAxisLineColor": "#57a532",
      "divlineColor": "#57a532",
      "divLineIsDashed": "1",
      "showAlternateHGridColor": "0",
      "subcaptionFontBold": "0",
      "subcaptionFontSize": "14"
     },
     "data": JSON.parse($('#value_dashboard').text())
//     "trendlines": [
//      {
//       "line": [
//        {
//         "startvalue": "700000",
//         "color": "#1aaf5d",
//         "valueOnRight": "1",
//         "displayvalue": "Monthly Target"
//        }
//       ]
//      }
//     ]
    }
   }).render();
  });
 }
};

$(function () {
 dashboard.setGrafik();
});