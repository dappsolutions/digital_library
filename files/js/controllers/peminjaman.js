var peminjaman = {
 module: function () {
  return 'peminjaman';
 },

 addBuku: function (elm) {
  var data = $(elm).closest('.controls');
  var new_data = data.clone();
  new_data.find('#buku').val('');
  data.after(new_data);
 },

 removeBuku: function (elm) {
  var totalBuku = $('.buku').length;
  console.log(totalBuku);
  if (totalBuku > 1) {
   console.log('remove');
   $(elm).closest('.controls').remove();
  }
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   peminjaman.show_dialog("Proses Mendapatkan Data..");
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: peminjaman.module() + '/search',
    success: function (resp) {
     $('#data_siswa_pinjam').html(resp);
     peminjaman.close_dialog();
    }
   });
  }
 },

 show_dialog: function (message) {
  bootbox.dialog({
   message: message
  });
 },

 close_dialog: function () {
  $('.bootbox-close-button').click();
 },

 getAutocompleteSiswa: function (elm) {
  $(elm).autocomplete({
   source: peminjaman.module() + "/getAutocompleteSiswa",
   minLength: 1,
   select: function (event, ui)
   {
    /*log(ui.item ?
     "Selected: " + ui.item.value + " aka " + ui.item.id :
     "Nothing selected, input was " + this.value);*/
   }
  });
 },

 getAutocompleteBuku: function (elm, e) {
  if (e.keyCode != 13) {
   $(elm).autocomplete({
    source: peminjaman.module() + "/getAutocompleteBuku",
    minLength: 1,
    select: function (event, ui)
    {
     /*log(ui.item ?
      "Selected: " + ui.item.value + " aka " + ui.item.id :
      "Nothing selected, input was " + this.value);*/
     $(elm).attr('no_induk', ui.item.no_induk_buku);
     $(elm).attr('id_buku', ui.item.id);
     console.log("buku", ui.item.value);

//     var buku = ui.item.no_induk_buku +" - "+ui.item.value;
//     $(elm).val(buku);
    }
   });
  } else {
   $.ajax({
    type: 'POST',
    data: {buku: $(elm).val()},
    dataType: 'json',
    async: false,
    url: peminjaman.module() + '/checkBukuAvailable',
    success: function (resp) {
     if (!resp.is_valid) {
      toastr.error("Buku Stock Habis");
     } else {
      $(elm).val(resp.no_induk_buku+" - "+resp.buku);
      $(elm).attr('no_induk', resp.no_induk_buku);
      $(elm).attr('id_buku', resp.id);
     }
    }
   });
  }
 },

 get_buku: function () {
  var buku = $('.buku');
  var data = [];
  $.each(buku, function () {
   var data_in = {
    'id': $(this).attr('id_buku'),
    'buku': $(this).val(),
    'no_induk_buku': $(this).attr('no_induk')
   };

   data.push(data_in);
  });

  return data;
 },

 get_post_data: function () {
  var data = {
   'nis': $('#nis').val(),
   'buku': peminjaman.get_buku()
  };
  return data;
 },

 save: function () {
  var data = peminjaman.get_post_data();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async: false,
   url: peminjaman.module() + '/save',
   success: function (resp) {
    if (resp.is_valid) {
     toastr.success('Peminjaman Berhasil Disimpan..');
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error('Peminjaman Gagal Disimpan..');
    }

    peminjaman.resetForm();
   }
  });
 },

 resetForm: function () {
  var input = $('input');
  $.each(input, function () {
   $(this).val('');
  });
 },

 detailPeminjaman: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: peminjaman.module() + '/detailPeminjaman/' + id,
   success: function (resp) {
    peminjaman.show_dialog(resp);
   }
  });
 },

 printPeminjaman: function (id) {
  window.open(peminjaman.module() + '/cetakFormulirPeminjaman/' + id);
 }
};

$(function () {

});