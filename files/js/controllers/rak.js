var rak = {
 module: function(){
  return 'rak';
 },

 show_dialog: function (message) {
  bootbox.dialog({
   message: message
  });
 },

 close_dialog: function () {
  $('.bootbox-close-button').click();
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   rak.show_dialog("Proses Pengambilan Data..");
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: rak.module() + '/search',
    success: function (resp) {
     $('#data_rak').html(resp);
     rak.close_dialog();
    }
   });
  }
 },

 edit: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: rak.module() + '/edit/' + id,
   success: function (resp) {
    rak.show_dialog(resp);
   }
  });
 },

 add: function () {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: rak.module() + '/add',
   success: function (resp) {
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 validation: function () {
  var required = $('.required');
  var empty = 0;
  var is_valid = 1;
  $.each(required, function () {
   var value = $(this).val();
   if (value == '') {
    empty += 1;
    $(this).after('<p style="color:red">*' + $(this).attr('error') + ' Harus Diisi</p>');
   }
  });

  if (empty > 0) {
   is_valid = 0;
  }

  return is_valid;
 },

 save: function () {
  if (rak.validation()) {
   rak.execSave();
  }
 },

 get_post_data: function () {
  var data = {
   'rak': $('#rak_data').val()
  };

  return data;
 },

 execSave: function () {
  var data = rak.get_post_data();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async: false,
   url: rak.module() + '/execSave',
   success: function (resp) {
    if (resp.is_valid) {
     toastr.success('Data Berhasil Disimpan');
    } else {
     toastr.error('Data Gagal Disimpan');
    }

    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 },

 exeUpdate: function () {
  var data = rak.get_post_data();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('id', $('#id').val());

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async: false,
   url: rak.module() + '/exeUpdate',
   success: function (resp) {
    if (resp.is_valid) {
     toastr.success('Data Berhasil Diperbaharui');
    } else {
     toastr.error('Data Gagal Diperbaharui');
    }

    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 },

 konfirmasi_delete: function (id, rak) {
  var html = '<div>';
  html += '<div class="text-center"><p class="center">Apakah anda yakin akan menghapus data ini ?</p></div>';
  html += "<div class='text-center'><p class='center'><button class='btn btn-success' rak='"+rak+"' onclick='rak.delete(this," + id + ")'>Ya</button>\n\
&nbsp;<button class='btn btn-warning' onclick='rak.close_dialog()'>Tidak</button></p></div>";
  html += '</div>';
  bootbox.dialog({
   message: html
  });
 },
 
 delete: function(elm, id){
  var rak = $(elm).attr('rak');
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: 'rak/delete/' + id + '/' + rak,
   success: function (resp) {
    if (resp.is_valid) {
     toastr.success('Data Berhasil Dihapus');
    } else {
     toastr.error('Data Gagal Dihapus');
    }

    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 }
};