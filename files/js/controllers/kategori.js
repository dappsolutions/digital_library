var kategori = {
 module: function(){
  return 'kategori';
 },
 
 add: function(){
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: kategori.module()+'/add',
   success: function (resp) {
    bootbox.dialog({
     message: resp
    });
   }
  });
 },
 
 show_dialog: function(message){
  bootbox.dialog({
   message: message
  });
 },
 
 close_dialog: function () {
  $('.bootbox-close-button').click();
 },
 
 search: function(elm, e){    
  if(e.keyCode == 13){
   kategori.show_dialog("Proses Pengambilan Data..");
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: kategori.module()+'/search',
    success: function (resp) {
     $('#data_kategori').html(resp);
     kategori.close_dialog();
    }
   });
  }
 },
 
 edit: function(id){
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: kategori.module()+'/edit/'+id,
   success: function (resp) {
    kategori.show_dialog(resp);
   }
  });
 },
 
 exeUpdate: function(){
  var data = kategori.get_post_data();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('id', $('#id').val());
  
  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async:false,
   url: kategori.module()+'/exeUpdate',
   success: function (resp) {
    if(resp.is_valid){
     toastr.success('Data Berhasil Diperbaharui');
    }else{
     toastr.error('Data Gagal Diperbaharui');
    }
    
    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 },
 
 konfirmasi_delete: function (id, kategori) {
  var html = '<div>';
  html += '<div class="text-center"><p class="center">Apakah anda yakin akan menghapus data ini ?</p></div>';
  html += "<div class='text-center'><p class='center'><button class='btn btn-success' kategori='"+kategori+"' onclick='kategori.delete(this," + id + ")'>Ya</button>\n\
&nbsp;<button class='btn btn-warning' onclick='kategori.close_dialog()'>Tidak</button></p></div>";
  html += '</div>';
  bootbox.dialog({
   message: html
  });
 },
 
 validation:function(){
  var required = $('.required');
  var empty = 0;
  var is_valid = 1;
  $.each(required, function () {
   var value = $(this).val();
   if(value == ''){
    empty+=1;
    $(this).after('<p style="color:red">*'+$(this).attr('error')+' Harus Diisi</p>');
   }
  });  
  
  if(empty > 0){
   is_valid = 0;
  }
  
  return is_valid;
 },
 
 save: function(){
  if(kategori.validation()){
   kategori.execSave();
  }
 },
 
 get_post_data: function(){
  var data = {
   'kategori': $('#kategori_data').val()
  };
  
  return data;
 },
 
 execSave: function(){
  var data = kategori.get_post_data(); 
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async:false,
   url: kategori.module()+'/execSave',
   success: function (resp) {
    if(resp.is_valid){
     toastr.success('Data Berhasil Disimpan');     
    }else{
     toastr.error('Data Gagal Disimpan');
    }
    
    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 },
 
 delete: function(elm, id){
  var kategori = $(elm).attr('kategori');

  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: 'kategori/delete/' + id + '/' + kategori,
   success: function (resp) {
    if (resp.is_valid) {
     toastr.success('Data Berhasil Dihapus');
    } else {
     toastr.error('Data Gagal Dihapus');
    }

    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 }
};