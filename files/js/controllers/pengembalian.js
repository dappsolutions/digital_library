var pengembalian = {
 module: function () {
  return 'pengembalian';
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   pengembalian.show_dialog("Proses Mendapatkan Data..");
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: pengembalian.module() + '/search',
    success: function (resp) {
     $('#data_siswa_pinjam').html(resp);
     pengembalian.close_dialog();
    }
   });
  }
 },

 show_dialog: function (message) {
  bootbox.dialog({
   message: message
  });
 },

 close_dialog: function () {
  $('.bootbox-close-button').click();
 },

 getAutocompleteSiswaPinjam: function (elm, e) {
  if (e.keyCode == 13) {
   pengembalian.show_dialog('Proses Mendapatkan Data');
   $.ajax({
    type: 'POST',
    dataType: 'json',
    async: false,
    url: pengembalian.module() + '/getAutocompleteSiswaPinjam/' + $(elm).val(),
    error: function () {
     toastr.error('Data Tidak Ditemukan');
     pengembalian.close_dialog();
    },

    success: function (resp) {
     if (resp.is_valid) {
      $('.list_buku').html(resp.view);
     } else {
      toastr.error(resp.view);
     }
     pengembalian.close_dialog();
    }
   });
  }
 },

 get_buku: function () {
  var buku = $('.buku');
  var data = [];
  $.each(buku, function () {
   var data_in = {
    'buku': $(this).attr('buku_id'),
    'hilang': $(this).siblings('#is_lost').is(':checked') ? 1 : 0
   };

   data.push(data_in);
  });

  return data;
 },

 get_post_data: function () {
  var data = {
   'nis': $('#nis').val(),
   'transaksi_peminjaman': $('#transaksi_peminjaman').val(),
   'tanggal_pinjam': $('#tanggal_pinjam').val(),
   'buku': pengembalian.get_buku()
  };
  return data;
 },

 save: function () {
  var data = pengembalian.get_post_data();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async: false,
   url: pengembalian.module() + '/save',
   success: function (resp) {
    if (resp.is_valid) {
     toastr.success('Peminjaman Berhasil Disimpan..');
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);

     pengembalian.resetForm();
    } else {
     toastr.error('Peminjaman Gagal Disimpan..');
    }
   }
  });
 },

 resetForm: function () {
  var input = $('input');
  $.each(input, function () {
   $(this).val('');
  });
 },

 detailPeminjaman: function (id, color) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: pengembalian.module() + '/detailPeminjaman/' + id + '/' + color,
   success: function (resp) {
    pengembalian.show_dialog(resp);
   }
  });
 }
};

$(function () {

});