

var buku = {
 module: function () {
  return 'buku';
 },

 add: function () {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(buku.module()) + 'add',
   success: function (resp) {
    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 show_dialog: function (message) {
  bootbox.dialog({
   message: message
  });
 },

 close_dialog: function () {
  $('.bootbox-close-button').click();
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord == "") {
    window.location.href = url.base_url(buku.module()) + "index"
   } else {
    window.location.href = url.base_url(buku.module()) + "search" + '/' + keyWord;
   }
//   buku.show_dialog("Proses Pengambilan Data..");
//   var keyword = $(elm).val();
//   var limit = $('#limit').val();
//   var total_segmen = $('#total_segmen').val();
//   $.ajax({
//    type: 'POST',
//    data: {keyword: keyword, limit : limit, total_segmen: total_segmen},
//    dataType: 'json',
//    async: false,
//    url: buku.module()+'/search',
//    success: function (resp) {
//     if($(elm).val() != ''){
//      $('#data_buku').html(resp.view);
//      $('.pagination').html(resp.pagination);
//     }else{
//      window.location.reload();
//     }
//    }
//   });
  }
 },

 edit: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(buku.module()) + '/edit/' + id,
   success: function (resp) {
    buku.show_dialog(resp);
   }
  });
 },

 showBarcode: function (no_induk_buku) {
  window.open(url.base_url(buku.module()) + "showBarcode/" + no_induk_buku);
 },

 exeUpdate: function (id) {
  var data = buku.get_post_data();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('id', id);
  formData.append('buku_hilang', $('#buku_hilang').val());
  formData.append('buku_keluar', $('#buku_keluar').val());

  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async: false,
   url: url.base_url(buku.module()) + 'exeUpdate',
   success: function (resp) {
    if (resp.is_valid) {
     toastr.success('Data Berhasil Diperbaharui');
    } else {
     toastr.error('Data Gagal Diperbaharui');
    }

    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 },

 konfirmasi_delete: function (id, buku) {
  var html = '<div>';
  html += '<div class="text-center"><p class="center">Apakah anda yakin akan menghapus data ini ?</p></div>';
  html += "<div class='text-center'><p class='center'><button class='btn btn-success' buku='" + buku + "' onclick='buku.delete(this," + id + ")'>Ya</button>\n\
&nbsp;<button class='btn btn-warning' onclick='buku.close_dialog()'>Tidak</button></p></div>";
  html += '</div>';
  bootbox.dialog({
   message: html
  });
 },

 validation: function () {
  var required = $('.required');
  var empty = 0;
  var is_valid = 1;
  $.each(required, function () {
   var value = $(this).val();
   if (value == '') {
    empty += 1;
    $(this).after('<p style="color:red">*' + $(this).attr('error') + ' Harus Diisi</p>');
   }
  });

  if (empty > 0) {
   is_valid = 0;
  }

  return is_valid;
 },

 save: function () {
  if (buku.validation()) {
   buku.execSave();
  }
 },

 get_post_data: function () {
  var data = {
   'no_induk_buku': $('#no_induk_buku').val(),
   'nama': $('#nama').val(),
   'kategori': $('#kategori').val(),
   'rak': $('#rak').val(),
   'judul': $('#judul').val(),
   'harga': $('#harga').val(),
   'keterangan': $('#keterangan').val(),
   'denda': $('#denda').val(),
   'stock': $('#stock').val(),
   'pengarang': $('#pengarang').val()
  };

  return data;
 },

 execSave: function () {
  var data = buku.get_post_data();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async: false,
   url: url.base_url(buku.module()) + 'execSave',
   success: function (resp) {
    if (resp.is_valid) {
     toastr.success('Data Berhasil Disimpan');
    } else {
     toastr.error('Data Gagal Disimpan');
    }

    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 },

 delete: function (elm, id) {
  var buku = $(elm).attr('buku');

  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(buku.module()) + 'delete/' + id + '/' + buku,
   success: function (resp) {
    if (resp.is_valid) {
     toastr.success('Data Berhasil Dihapus');
    } else {
     toastr.error('Data Gagal Dihapus');
    }

    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 },

 printBarcode: function () {
  console.log(window.location.href);
 }
};

$(function () {
 buku.printBarcode();
});