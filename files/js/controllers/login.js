var login = {
 sign_in: function () {
  var username = $('#username').val();
  var password = $('#password').val();

  $.ajax({
   type: 'POST',
   data: {username: username, password: password},
   dataType: 'json',
   async: false,
   url: 'login/sign_in',
   success: function (resp) {
    if(resp.is_valid){
     toastr.success('Login Berhasil...');
     var home = function () {
      window.location = "home";
     };
     setTimeout(home(), 1000);
    }else{
     toastr.error('Login Gagal...');
    }        
   }
  });
 }
};