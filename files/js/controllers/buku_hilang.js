var buku_hilang = {
 module: function(){
  return 'buku_hilang';
 },
 
 show_dialog: function(message){
  bootbox.dialog({
   message: message
  });
 },
 
 close_dialog: function () {
  $('.bootbox-close-button').click();
 },
 
 search: function(elm, e){    
  if(e.keyCode == 13){
   buku_hilang.show_dialog("Proses Pengambilan Data..");
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: buku_hilang.module()+'/search',
    success: function (resp) {
     $('#data_buku_hilang').html(resp);
     buku_hilang.close_dialog();
    }
   });
  }
 }
};