var pembayaran_denda = {
 module: function () {
  return 'pembayaran_denda';
 },

 show_dialog: function (message) {
  bootbox.dialog({
   message: message
  });
 },

 close_dialog: function () {
  $('.bootbox-close-button').click();
 },

 //untuk menampilkan tooltip
 show_tooltip: function (elm) {
  $(elm).tooltip({placement: 'bottom'});
 },

 bayar_denda_buku_hilang: function (transaksi_denda_buku_hilang) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   contentType: false,
   processData: false,
   url: pembayaran_denda.module() + '/bayar_denda_buku_hilang/' + transaksi_denda_buku_hilang,
   success: function (resp) {
    if(resp.is_valid){
     toastr.success('Pembayaran Berhasil Dilakukan');
     var reload = function(){
      window.location.reload();
     };
     
     setTimeout(reload(), 1000);
    }else{
     toastr.success('Pembayaran Gagal Dilakukan');
    }
   }
  });
 },
 
 bayar_denda: function (transaksi_denda) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   contentType: false,
   processData: false,
   url: pembayaran_denda.module() + '/bayar_denda/' + transaksi_denda,
   success: function (resp) {
    if(resp.is_valid){
     toastr.success('Pembayaran Berhasil Dilakukan');
     var reload = function(){
      window.location.reload();
     };
     
     setTimeout(reload(), 1000);
    }else{
     toastr.success('Pembayaran Gagal Dilakukan');
    }
   }
  });
 },
 
 search: function(elm, e){    
  if(e.keyCode == 13){
   pembayaran_denda.show_dialog("Proses Pengambilan Data..");
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: pembayaran_denda.module()+'/search',
    success: function (resp) {
     $('#data_pembayaran_denda').html(resp);
     pembayaran_denda.close_dialog();
    }
   });
  }
 },
};