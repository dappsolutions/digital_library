var buku_pinjam = {
 module: function () {
  return 'buku_pinjam';
 },

 show_dialog: function (message) {
  bootbox.dialog({
   message: message
  });
 },

 close_dialog: function () {
  $('.bootbox-close-button').click();
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   buku_pinjam.show_dialog("Proses Pengambilan Data..");
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: buku_pinjam.module() + '/search',
    success: function (resp) {
     $('#data_buku_pinjam').html(resp);
     buku_pinjam.close_dialog();
    }
   });
  }
 }
};