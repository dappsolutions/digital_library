var absensi_siswa = {
 module: function () {
  return 'absensi_siswa';
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   absensi_siswa.show_dialog("Proses Mendapatkan Data..");
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: absensi_siswa.module() + '/search',
    success: function (resp) {
     $('#data_siswa_masuk').html(resp);
     absensi_siswa.close_dialog();
    }
   });
  }
 },

 resetForm: function () {
  var input = $('input');
  $.each(input, function () {
   $(this).val('');
  });
 },
 
 show_dialog: function (message) {
  bootbox.dialog({
   message: message
  });
 },

 close_dialog: function () {
  $('.bootbox-close-button').click();
 },

 doAbsensi: function (elm, e) {
  if (e.keyCode == 13) {
   absensi_siswa.show_dialog('Proses Mendapatkan Data');
   $.ajax({
    type: 'POST',
    dataType: 'json',
    async: false,
    url: absensi_siswa.module() + '/doAbsensi/' + $(elm).val(),
    error: function () {
     toastr.error('Data Tidak Ditemukan');
     absensi_siswa.close_dialog();
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Absensi Berhasil Dilakukan");
      var reload = function () {
       window.location.reload();
      };
      setTimeout(reload(), 1000);
     } else {
      toastr.error("Absensi Gagal Dilakukan, \n\
Karena Siswa Belu Terdaftar");
     }
     absensi_siswa.close_dialog();
     absensi_siswa.resetForm();
    }
   });
  }
 },
};

$(function () {

});