var range_peminjaman = {
 module: function(){
  return 'range_peminjaman';
 },
 
 add: function(){
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: range_peminjaman.module()+'/add',
   success: function (resp) {
    bootbox.dialog({
     message: resp
    });
   }
  });
 },
 
 show_dialog: function(message){
  bootbox.dialog({
   message: message
  });
 },
 
 close_dialog: function () {
  $('.bootbox-close-button').click();
 },
 
 search: function(elm, e){    
  if(e.keyCode == 13){
   range_peminjaman.show_dialog("Proses Pengambilan Data..");
   var keyword = $(elm).val();
   $.ajax({
    type: 'POST',
    data: {keyword: keyword},
    dataType: 'html',
    async: false,
    url: range_peminjaman.module()+'/search',
    success: function (resp) {
     $('#data_range_peminjaman').html(resp);
     range_peminjaman.close_dialog();
    }
   });
  }
 },
 
 edit: function(id){
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: range_peminjaman.module()+'/edit/'+id,
   success: function (resp) {
    range_peminjaman.show_dialog(resp);
   }
  });
 },
 
 exeUpdate: function(){
  var data = range_peminjaman.get_post_data();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('id', $('#id').val());
  
  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async:false,
   url: range_peminjaman.module()+'/exeUpdate',
   success: function (resp) {
    if(resp.is_valid){
     toastr.success('Data Berhasil Diperbaharui');
    }else{
     toastr.error('Data Gagal Diperbaharui');
    }
    
    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 },
 
 validation:function(){
  var required = $('.required');
  var empty = 0;
  var is_valid = 1;
  $.each(required, function () {
   var value = $(this).val();
   if(value == ''){
    empty+=1;
    $(this).after('<p style="color:red">*'+$(this).attr('error')+' Harus Diisi</p>');
   }
  });  
  
  if(empty > 0){
   is_valid = 0;
  }
  
  return is_valid;
 },
 
 save: function(){
  if(range_peminjaman.validation()){
   range_peminjaman.execSave();
  }
 },
 
 get_post_data: function(){
  var data = {
   'jumlah_hari': $('#jumlah_hari_data').val()
  };
  
  return data;
 },
 
 execSave: function(){
  var data = range_peminjaman.get_post_data(); 
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   contentType: false,
   processData: false,
   async:false,
   url: range_peminjaman.module()+'/execSave',
   success: function (resp) {
    if(resp.is_valid){
     toastr.success('Data Berhasil Disimpan');     
    }else{
     toastr.error('Data Gagal Disimpan');
    }
    
    var reload = function () {
     window.location.reload();
    };
    setTimeout(reload(), 1000);
   }
  });
 }
};