var Pagination = {
 module: function () {
  return $('#module_name').val();
 },

 setActiveChoosePage: function (elm) {
  var offset = $(elm).text();
  var html = '<td colspan="30">Proses Pengambilan Data Halaman ' + offset + '...</td>';
  $('.data-table').find('tbody').find('tr').html(html);
  $('li.active').removeClass('active');
  $(elm).closest('li').addClass('active');
 },

 setActiveChoosePageNext: function (offset) {
  var halaman = offset;
  offset -= 1;
  var html = '<td colspan="30">Proses Pengambilan Data Halaman ' + halaman + '...</td>';
  $('.data-table').find('tbody').find('tr').html(html);
  $('li.active').removeClass('active');
  $('div.pagination').find('li:eq(' + offset + ')').addClass('active');
 },

 setActiveChoosePagePrev: function (offset) {
  var halaman = offset;
  var html = '<td colspan="30">Proses Pengambilan Data Halaman ' + halaman + '...</td>';
  $('.data-table').find('tbody').find('tr').html(html);
  $('li.active').removeClass('active');
  $.each($('div.pagination').find('li'), function () {
   if($(this).find('a').text() == offset){
    $(this).addClass('active');
   }
  });
//  $('div.pagination').find('li:eq(' + offset + ')').addClass('active');
 },

 getData: function (elm) {
  var offset = $(elm).text();
  var limit = $('#limit').val();

  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Pagination.module()) + 'getDataFromPagination' + '/' + offset + '/' + limit,
   beforeSend: function () {
    Pagination.setActiveChoosePage(elm);
   },

   success: function (resp) {
    $('.data').html(resp);
    $(Pagination.updatedPageNumber(elm));
   }
  });
 },

 updatedPageNumber: function (elm) {
  var offset = $(elm).text();
  var total_segmen = $('#total_segmen').val();
  var segmen = $('#segmen').val();
  var no_active = $('div.pagination').find('li:eq(' + (total_segmen) + ')').find('a').text();
  var prev = $('.prev').length;

  var selisih_data = segmen - offset;
 },

 gotoPrevPage: function (elm) {
  var total_segmen = $('#total_segmen').val();
  var offset = parseInt($('div.pagination').find('li.active').find('a').text());
  offset -= 1;
  var limit = $('#limit').val();
  
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Pagination.module()) + 'getDataFromPagination' + '/' + offset + '/' + limit,
   beforeSend: function () {
    if (offset > 1) {
    Pagination.setActiveChoosePagePrev(offset);
    }else{
     var html = '<td colspan="30">Proses Pengambilan Data Halaman ' + offset + '...</td>';
     $('.data-table').find('tbody').find('tr').html(html);
    }
   },

   success: function (resp) {    
//    if (offset == 1) {
     $(Pagination.prevPage(elm, offset));
//    }else{
//     Pagination.setActiveChoosePagePrev(offset);
//    }
    $('.data').html(resp);
   }
  });
 },

 gotoNextPage: function (elm) {
  var total_segmen = $('#total_segmen').val();
  var offset;
  if($('.prev').length == 0){
   offset = parseInt($('div.pagination').find('li:eq(' + (total_segmen - 1) + ')').find('a').text());
  }else{
   offset = parseInt($('div.pagination').find('li.active').find('a').text());
  }  
  
  offset += 1;    
  var limit = $('#limit').val();
  var active_page = $('div.pagination').find('li.active').find('a').text();
  var selisih_data = offset - active_page;  

  if (selisih_data > 1) {
   offset -= 1;
  }

  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Pagination.module()) + 'getDataFromPagination' + '/' + offset + '/' + limit,
   beforeSend: function () {
    if (selisih_data > 1) {
     Pagination.setActiveChoosePageNext(offset);
    } else {
     var html = '<td colspan="30">Proses Pengambilan Data Halaman ' + offset + '...</td>';
     $('.data-table').find('tbody').find('tr').html(html);
    }
   },

   success: function (resp) {
    $('.data').html(resp);
    if (selisih_data == 1) {
     $(Pagination.nextPage(elm, offset));
    }
   }
  });
 },

 nextPage: function (elm, offset) {
  var total_segmen = $('#total_segmen').val();
  var segmen = $('#segmen').val();
  var active_page = $('div.pagination').find('li.active').find('a').text();
  var last_page = 0;

  var selisih_data = offset - active_page;
  if (selisih_data == 1) {
   $.each($('.pagination').find('li'), function () {
    if (!isNaN($(this).text())) {
     var no_page = parseInt($(this).text()) + 1;
     if (no_page == offset) {
      $('li.active').removeClass('active');
      $(this).addClass('active');
     }
     $(this).find('a').text(no_page);
     last_page = no_page;
    }
   });

   $('li.prev').remove();
   var prev_page = '<li class="prev"><a href="#" onclick="Pagination.gotoPrevPage(this)">Prev</a></li>';
   var ul = $('.pagination').find('li:eq(0)').closest('ul');
   ul.prepend(prev_page);

//   console.log(last_page + ' dan ' + segmen);
   if (last_page == segmen) {
    $('.next').remove();
   }
  }
 },

 prevPage: function (elm, offset) {
  var total_segmen = $('#total_segmen').val();
  var segmen = $('#segmen').val();
  var active_page = $('div.pagination').find('li.active').find('a').text();
//  var first_page = $('div.pagination').find('li:eq(1)').find('a').text();
  
  var selisih_data = 0;
  if(active_page != ''){
   selisih_data = active_page - offset;
  }else{
   selisih_data = 1;
  }  
  
  console.log('selisih',selisih_data);
  console.log('active',active_page);
  console.log('offset',offset);
  if (selisih_data == 1) {
   $.each($('.pagination').find('li'), function () {
    if (!isNaN($(this).text())) {
     var no_page = parseInt($(this).text()) - 1;
     if (no_page == offset) {
      $('li.active').removeClass('active');
      $(this).addClass('active');
     }
     $(this).find('a').text(no_page);
    }
   });

   $('li.next').remove();
   var next_page = '<li class="next"><a href="#" onclick="Pagination.gotoNextPage(this)">Next</a></li>';
   var ul = $('.pagination').find('li:eq(0)').closest('ul');
   ul.append(next_page);
   
   var first_page = $('div.pagination').find('li.active').find('a').text();
   if (first_page == 1) {
    $('.prev').remove();
   }
  }
 }
};