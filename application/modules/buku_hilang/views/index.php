<script src="<?php echo base_url() ?>files/js/controllers/buku_hilang.js"></script> 
<div id="content-header">
 <div id="breadcrumb"> <a href="<?php echo base_url() . 'home' ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
  <a href="<?php echo base_url() . 'buku_hilang' ?>" class="current">Buku Hilang</a> </div>
 <h1>Daftar Buku Hilang</h1>
</div>
<div class="container-fluid">
 <div class="row-fluid">
  <div class="span12">
   <div class="row-fluid">
    <div class="span6">
     <div class="control-group">
      <div class="controls">
       <input class="span6" placeholder="Pencarian Data" type="text" id="" onkeypress="buku_hilang.search(this, event)">
      </div>
     </div>
    </div>
   </div>
   <div class="widget-box">
    <div class="widget-title">
     <span class="icon"><i class="icon-th"></i></span> 
     <h5>Daftar Buku Hilang</h5>
    </div>
    <div class="widget-content nopadding" style="overflow: auto;max-height: 400px;" id="data_buku_hilang">
     <table class="table table-bordered data-table">
      <thead>
       <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Kategori</th>
        <th>Rak</th>
        <th>Judul</th>
        <th>Pengarang</th>
        <th>Harga</th>
        <th>Keterangan</th>
        <th>Harga Denda Buku</th>
       </tr>
      </thead>
      <tbody>
       <?php if (!empty($data)) { ?>
        <?php $no = 1; ?>
        <?php foreach ($data as $value) { ?>
         <tr class="text-center">
          <td class="center"><?php echo $no++ ?></td>
          <td class="center"><?php echo $value['nama'] ?></td>
          <td class="center"><?php echo $value['kategori'] ?></td>
          <td class="center"><?php echo $value['rak'] ?></td>
          <td class="center"><?php echo $value['judul'] ?></td>
          <td class="center"><?php echo $value['pengarang'] ?></td>
          <td class="center"><?php echo number_format($value['harga'], 2, ',', '.') ?></td>
          <td class="center"><?php echo $value['keterangan'] ?></td>
          <td class="center"><?php echo number_format($value['denda'], 2, ',', '.') ?></td>
         </tr>
        <?php } ?>
       <?php } else { ?>
        <tr class="text-center">
         <td class="center" colspan="9">Tidak Ada Data</td>
        </tr>
       <?php } ?>
      </tbody>
     </table>
    </div>
   </div>

  </div>
 </div>
</div>