<table class="table table-bordered data-table">
 <thead>
  <tr>
   <th>No</th>
   <th>Nama</th>
   <th>Kategori</th>
   <th>Rak</th>
   <th>Judul</th>
   <th>Pengarang</th>
   <th>Harga</th>
   <th>Keterangan</th>
   <th>Harga Denda Buku</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($data)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($data as $value) { ?>
    <tr class="text-center">
     <td class="center"><?php echo $no++ ?></td>
     <td class="center"><?php echo $value['nama'] ?></td>
     <td class="center"><?php echo $value['kategori'] ?></td>
     <td class="center"><?php echo $value['rak'] ?></td>
     <td class="center"><?php echo $value['judul'] ?></td>
     <td class="center"><?php echo $value['pengarang'] ?></td>
     <td class="center"><?php echo number_format($value['harga'], 2, ',', '.') ?></td>
     <td class="center"><?php echo $value['keterangan'] ?></td>
     <td class="center"><?php echo number_format($value['denda'], 2, ',', '.') ?></td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr class="text-center">
    <td class="center" colspan="9">Tidak Ada Data</td>
   </tr>
  <?php } ?>
 </tbody>
</table>