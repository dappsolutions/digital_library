<?php

class Buku_hilang extends MX_Controller {

 public function __construct() {
  parent::__construct();
  if ($this->session->userdata('user') == "") {
   redirect(base_url());
  }
 }

 public function get_module() {
  return "buku_hilang";
 }

 public function get_table_name() {
  return 'buku';
 }
 
 public function index() {
  $data['module'] = $this->get_module();
  $data['view'] = 'index';
  $data['data'] = $this->getDataBuku();
  echo Modules::run('template', $data);
 }

 public function getDataBuku() {
  $data = Modules::run('database/get', array(
    'table' => $this->get_table_name().' buk',
    'field' => array('buk.id', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul', 
     'buk.harga', 'buk.keterangan', 'buk.denda', 'buk.pengarang'),
    'join' => array(
     array('kategori kat', 'buk.kategori = kat.id'),
     array('rak rk', 'buk.rak = rk.id'),
     array('buku_hilang bh', 'buk.id = bh.buku')
    )
  ));
  
  $result = array();
  if(!empty($data)){
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  return $result;
 }
 
 public function search() {
  $keyword = rawurldecode($this->input->post('keyword'));
  $data = Modules::run('database/get', array(
    'table' => $this->get_table_name().' buk',
    'field' => array('buk.id', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul', 
     'buk.harga', 'buk.keterangan', 'buk.denda', 'buk.pengarang'),
    'join' => array(
     array('kategori kat', 'buk.kategori = kat.id'),
     array('rak rk', 'buk.rak = rk.id'),
     array('buku_hilang bh', 'buk.id = bh.buku')
    ),
    'is_or_like'=> true,
    'like'=> array(
     array('buk.id', $keyword),
     array('buk.nama', $keyword),
     array('kat.kategori', $keyword),
     array('rk.rak', $keyword),
     array('buk.judul', $keyword),
     array('buk.harga', $keyword),
     array('buk.keterangan', $keyword),
     array('buk.pengarang', $keyword),
     array('buk.denda', $keyword)
    )
  ));
  
  $result = array();
  if(!empty($data)){
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  $content['data'] = $result;
  echo $this->load->view('search', $content, true);
 } 
}
