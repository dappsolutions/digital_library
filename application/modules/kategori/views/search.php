<table class="table table-bordered data-table">
 <thead>
  <tr>
   <th>No</th>
   <th>Kategori</th>
   <th>Action</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($data)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($data as $value) { ?>
    <tr class="text-center">
     <td class="center"><?php echo $no++ ?></td>
     <td class="center"><?php echo $value['kategori'] ?></td>
     <td class="center">
      <i class="mdi mdi-pencil-circle mdi-24px" onclick="kategori.edit('<?php echo $value['id'] ?>')"></i>
      <i class="mdi mdi-delete mdi-24px" onclick="kategori.konfirmasi_delete('<?php echo $value['id'] ?>', '<?php echo $value['kategori'] ?>')"></i>
     </td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr class="text-center">
    <td class="center" colspan="3">Tidak Ada Data</td>
   </tr>
  <?php } ?>
 </tbody>
</table>