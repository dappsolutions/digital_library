<?php

class Kategori extends MX_Controller {

 public function __construct() {
  parent::__construct();
  if ($this->session->userdata('user') == "") {
   redirect(base_url());
  }
 }

 public function get_module() {
  return "kategori";
 }

 public function get_table_name() {
  return 'kategori';
 }
 
 public function index() {
  $data['module'] = $this->get_module();
  $data['view'] = 'index';
  $data['data'] = $this->getDataKategori();
  echo Modules::run('template', $data);
 }

 public function getDataKategori() {
  $data = Modules::run('database/get', array(
    'table' => $this->get_table_name()
  ));
  
  $result = array();
  if(!empty($data)){
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  return $result;
 }
 
 public function search() {
  $keyword = rawurldecode($this->input->post('keyword'));
  $data = Modules::run('database/get', array(
    'table' => $this->get_table_name(),
    'is_or_like'=> true,
    'like'=> array(
     array('kategori', $keyword)
    )
  ));
  
  $result = array();
  if(!empty($data)){
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  $content['data'] = $result;
  echo $this->load->view('search', $content, true);
 }
 
 public function add() {
  echo $this->load->view('add', array(), true);
 }
 
 public function get_post_data($value) {
  $data['kategori'] = $value->kategori;
  
  return $data;
 }
 
 public function execSave() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Tambah Kategori');
   $data_kategori = $this->get_post_data($data);
   $kategori = Modules::run('database/_insert', $this->get_table_name(), $data_kategori);
   Modules::run('helper/_insert_change_log', $this->get_table_name(), $kategori, $event);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  
  echo json_encode(array('is_valid'=> $is_valid));
 }
 
 public function edit($id) {
  $data = Modules::run('database/get', array(
    'table' => $this->get_table_name(),
    'where' => array('id'=> $id)
  ))->row_array();
  
  echo $this->load->view('edit', $data, true);
 }
 
 public function exeUpdate() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Update Kategori');
   $data_kategori = $this->get_post_data($data);
   $siswa = Modules::run('database/_update', $this->get_table_name(), $data_kategori, array('id'=> $id));
   Modules::run('helper/_insert_change_log', $this->get_table_name(), $id, $event);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  
  echo json_encode(array('is_valid'=> $is_valid));
 }
 
 public function delete($id, $kategori) {  
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Delete Kategori : '.$kategori);
   $siswa = Modules::run('database/_delete', $this->get_table_name(), array('id'=> $id));
   Modules::run('helper/_insert_change_log', $this->get_table_name(), $id, $event);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  
  echo json_encode(array('is_valid'=> $is_valid));
 }
}
