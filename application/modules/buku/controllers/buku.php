<?php

class Buku extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 25;
  if ($this->session->userdata('user') == "") {
   redirect(base_url());
  }
 }

 public function get_module() {
  return "buku";
 }

 public function get_table_name() {
  return 'buku';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['module'] = $this->get_module();
  $data['view'] = 'index';
  $content = $this->getDataBuku();
//  echo '<pre>';
//  print_r($content);die;
  $data['data'] = $content['data'];
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->get_module() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function showBarcode($no_buku) {
  $data['no_induk_buku'] = rawurldecode($no_buku);
  echo $this->load->view('showBarcode', $data, true);
 }

// public function getDataBuku() {
//  $data = Modules::run('database/get', array(
//  'table' => $this->get_table_name() . ' buk',
//  'field' => array('buk.id', 'buk.no_induk_buku', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul',
//  'buk.harga', 'buk.keterangan', 'buk.denda', 'buk.pengarang',
//  'buk.buku_keluar as buku_pinjam', 'buk.stock', 'buk.buku_hilang'),
//  'join' => array(
//  array('kategori kat', 'buk.kategori = kat.id'),
//  array('rak rk', 'buk.rak = rk.id')
//  )
//  ));
//
//  $result = array();
//  if (!empty($data)) {
//   foreach ($data->result_array() as $value) {
//    array_push($result, $value);
//   }
//  }
//
//  return $result;
// }

 public function getDataBuku($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('buk.no_induk_buku', $keyword),
       array('buk.nama', $keyword),
   );
  }

  switch ($keyword) {
   case "":
    $data = Modules::run('database/get', array(
                'table' => $this->get_table_name() . ' buk',
                'field' => array('buk.id', 'buk.no_induk_buku', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul',
                    'buk.harga', 'buk.keterangan', 'buk.denda', 'buk.pengarang',
                    'buk.buku_keluar as buku_pinjam', 'buk.stock', 'buk.buku_hilang'),
                'join' => array(
                    array('kategori kat', 'buk.kategori = kat.id'),
                    array('rak rk', 'buk.rak = rk.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
    ));
    break;
   default:
    $data = Modules::run('database/get', array(
                'table' => $this->get_table_name() . ' buk',
                'field' => array('buk.id', 'buk.no_induk_buku', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul',
                    'buk.harga', 'buk.keterangan', 'buk.denda', 'buk.pengarang',
                    'buk.buku_keluar as buku_pinjam', 'buk.stock', 'buk.buku_hilang'),
                'join' => array(
                    array('kategori kat', 'buk.kategori = kat.id'),
                    array('rak rk', 'buk.rak = rk.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true
    ));
    break;
  }

  
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataBuku($keyword)
  );
 }

 public function getTotalDataBuku($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('buk.no_induk_buku', $keyword),
       array('buk.nama', $keyword),
   );
  }


  switch ($keyword) {
   case "":
    $total = Modules::run('database/count_all', array(
                'table' => $this->get_table_name() . ' buk',
                'field' => array('buk.id', 'buk.no_induk_buku', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul',
                    'buk.harga', 'buk.keterangan', 'buk.denda', 'buk.pengarang',
                    'buk.buku_keluar as buku_pinjam', 'buk.stock', 'buk.buku_hilang'),
                'join' => array(
                    array('kategori kat', 'buk.kategori = kat.id'),
                    array('rak rk', 'buk.rak = rk.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
    ));
    break;
   default:
    $total = Modules::run('database/count_all', array(
                'table' => $this->get_table_name() . ' buk',
                'field' => array('buk.id', 'buk.no_induk_buku', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul',
                    'buk.harga', 'buk.keterangan', 'buk.denda', 'buk.pengarang',
                    'buk.buku_keluar as buku_pinjam', 'buk.stock', 'buk.buku_hilang'),
                'join' => array(
                    array('kategori kat', 'buk.kategori = kat.id'),
                    array('rak rk', 'buk.rak = rk.id')
                ),
                'like' => $like,
                'is_or_like' => true,
                'limit' => $this->limit,
                'offset' => $this->last_no,
                'inside_brackets' => true
    ));
    break;
  }


  return $total;
 }

 public function getDataFromPagination($offset, $limit) {
  $query = "SELECT `buk`.`id`, `buk`.`no_induk_buku`, `buk`.`nama`, 
  `kat`.`kategori`, `rk`.`rak`, `buk`.`judul`, `buk`.`harga`, 
  `buk`.`keterangan`, `buk`.`denda`, `buk`.`pengarang`, `buk`.`buku_keluar` as buku_pinjam, 
  `buk`.`stock`, `buk`.`buku_hilang`
  FROM (`buku` buk)
  JOIN `kategori` kat ON `buk`.`kategori` = `kat`.`id`
  JOIN `rak` rk ON `buk`.`rak` = `rk`.`id` ";
  $data = Modules::run('database/get_custom', $query, $limit, ($offset - 1));

//  if($offset == 3){
//   echo '<pre>';
//   echo $this->db->last_query();
//   die;
//  }
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  $content['data'] = $result;
  echo $this->load->view('search', $content, true);
 }

// public function search() {
//  $keyword = rawurldecode($this->input->post('keyword'));
//  $data = Modules::run('database/get', array(
//  'table' => $this->get_table_name() . ' buk',
//  'field' => array('buk.id', 'buk.no_induk_buku', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul',
//  'buk.harga', 'buk.keterangan', 'buk.denda',
//  'buk.pengarang', 'buk.buku_keluar as buku_pinjam',
//  'buk.stock', 'buk.buku_hilang'),
//  'join' => array(
//  array('kategori kat', 'buk.kategori = kat.id'),
//  array('rak rk', 'buk.rak = rk.id')
//  ),
//  'is_or_like' => true,
//  'like' => array(
//  array('buk.id', $keyword),
//  array('buk.no_induk_buku', $keyword),
//  array('buk.nama', $keyword),
//  array('kat.kategori', $keyword),
//  array('rk.rak', $keyword),
//  array('buk.judul', $keyword),
//  array('buk.harga', $keyword),
//  array('buk.keterangan', $keyword),
//  array('buk.pengarang', $keyword),
//  array('buk.denda', $keyword)
//  )
//  ));
//
//  $result = array();
//  if (!empty($data)) {
//   foreach ($data->result_array() as $value) {
//    array_push($result, $value);
//   }
//  }
//
//  $content['data'] = $result;
//  echo $this->load->view('search', $content, true);
// }

 public function getDataBukuSearch($limit = '') {
  $keyword = rawurldecode($this->input->post('keyword'));
  if ($limit != '') {
   $data = Modules::run('database/get', array(
               'table' => $this->get_table_name() . ' buk',
               'field' => array('buk.id', 'buk.no_induk_buku', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul',
                   'buk.harga', 'buk.keterangan', 'buk.denda', 'buk.pengarang',
                   'buk.buku_keluar as buku_pinjam', 'buk.stock', 'buk.buku_hilang'),
               'join' => array(
                   array('kategori kat', 'buk.kategori = kat.id'),
                   array('rak rk', 'buk.rak = rk.id')
               ),
               'is_or_like' => true,
               'like' => array(
                   array('buk.id', $keyword),
                   array('buk.nama', $keyword),
                   array('kat.kategori', $keyword),
                   array('rk.rak', $keyword),
                   array('buk.judul', $keyword),
                   array('buk.harga', $keyword),
                   array('buk.keterangan', $keyword),
                   array('buk.pengarang', $keyword),
                   array('buk.denda', $keyword)
               ),
               'limit' => $limit
   ));
  } else {
   $data = Modules::run('database/get', array(
               'table' => $this->get_table_name() . ' buk',
               'field' => array('buk.id', 'buk.no_induk_buku', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul',
                   'buk.harga', 'buk.keterangan', 'buk.denda', 'buk.pengarang',
                   'buk.buku_keluar as buku_pinjam', 'buk.stock', 'buk.buku_hilang'),
               'join' => array(
                   array('kategori kat', 'buk.kategori = kat.id'),
                   array('rak rk', 'buk.rak = rk.id')
               ),
               'is_or_like' => true,
               'like' => array(
                   array('buk.id', $keyword),
                   array('buk.nama', $keyword),
                   array('kat.kategori', $keyword),
                   array('rk.rak', $keyword),
                   array('buk.judul', $keyword),
                   array('buk.harga', $keyword),
                   array('buk.keterangan', $keyword),
                   array('buk.pengarang', $keyword),
                   array('buk.denda', $keyword)
               )
   ));
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function search($keyword = "") {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
//  $keyword = $this->session->userdata('keyword');
  $keyword = urldecode($keyword);
// echo $keyword;die;
  $data['keyword'] = $keyword;
  $data['view'] = 'index';
  $data['module'] = $this->get_module();
  $content = $this->getDataBuku($keyword);
  $data['data'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->get_module() . '/search/'.$keyword.'/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

// public function search() {
//  $keyword = rawurldecode($this->input->post('keyword'));
//  $limit = $this->input->post('limit');
//  $segmen = $this->input->post('total_segmen');
//
//  $data = $this->getDataBukuSearch($limit);
//  $pagination = Modules::run('pagination/setPaging', count($this->getDataBukuSearch()), $limit, $segmen, $this->get_module());
//  $content['data'] = $data;
//  $view = $this->load->view('search', $content, true);
//  echo json_encode(array(
//      'view' => $view,
//      'pagination' => $pagination
//  ));
// }

 public function getListRak() {
  $data = Modules::run('database/get', array(
              'table' => 'rak'
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->result_array();
  }

  return $result;
 }

 public function getListKetegori() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori'
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->result_array();
  }

  return $result;
 }

 public function add() {
  $data['list_kategori'] = $this->getListKetegori();
  $data['list_rak'] = $this->getListRak();
  echo $this->load->view('add', $data, true);
 }

 public function get_post_data($value, $total_stock = '') {
  $data['no_induk_buku'] = $value->no_induk_buku;
  $data['nama'] = $value->nama;
  $data['kategori'] = $value->kategori;
  $data['rak'] = $value->rak;
  $data['judul'] = $value->judul;
  $data['harga'] = $value->harga;
  $data['keterangan'] = $value->keterangan;
  $data['denda'] = $value->denda;
  $data['stock'] = $value->stock;
  $data['pengarang'] = $value->pengarang;

  if ($total_stock != '') {
   $data['stock'] = $value->stock - $total_stock;
  }

  return $data;
 }

 public function execSave() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Tambah Buku');
   $data_buku = $this->get_post_data($data);
   $buku = Modules::run('database/_insert', $this->get_table_name(), $data_buku);
   Modules::run('helper/_insert_change_log', $this->get_table_name(), $buku, $event);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function edit($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->get_table_name() . ' buk',
              'field' => array('buk.id', 'buk.no_induk_buku', 'buk.nama', 'kat.kategori', 'rk.rak', 'buk.judul',
                  'buk.harga', 'buk.keterangan',
                  'buk.denda', 'buk.pengarang', 'buk.stock', 'buk.buku_hilang',
                  'buk.buku_keluar'),
              'join' => array(
                  array('kategori kat', 'buk.kategori = kat.id'),
                  array('rak rk', 'buk.rak = rk.id')
              ),
              'where' => array('buk.id' => $id)
          ))->row_array();

  $data['list_kategori'] = $this->getListKetegori();
  $data['list_rak'] = $this->getListRak();
  echo $this->load->view('edit', $data, true);
 }

 public function exeUpdate() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $buku_hilang = $this->input->post('buku_hilang');
  $buku_keluar = $this->input->post('buku_keluar');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Update Buku');
   $data_buku = $this->get_post_data($data, ($buku_hilang + $buku_keluar));
   $buku = Modules::run('database/_update', $this->get_table_name(), $data_buku, array('id' => $id));
   Modules::run('helper/_insert_change_log', $this->get_table_name(), $id, $event);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function delete($id, $buku) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Delete Buku : ' . $buku);
   $siswa = Modules::run('database/_delete', $this->get_table_name(), array('id' => $id));
   Modules::run('helper/_insert_change_log', $this->get_table_name(), $id, $event);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
