<script src="<?php echo base_url() ?>files/js/controllers/buku.js"></script> 
<div id="content-header">
 <div id="breadcrumb"> <a href="<?php echo base_url() . 'home' ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
  <a href="<?php echo base_url() . 'buku' ?>" class="current">Buku</a> </div>
 <h1>Daftar Buku</h1>
</div>
<div class="container-fluid">
 <div class="row-fluid">
  <div class="span12">
   <div class="row-fluid">
    <div class="span6">
     <button class="btn btn-success" id="" onclick="buku.add()">Tambah</button>
    </div>
    <div class="span6 text-right">
     <div class="control-group">
      <div class="controls">
       <input class="span6" placeholder="Pencarian Data" type="text" id="" onkeypress="buku.search(this, event)">
      </div>
     </div>
    </div>
   </div>
   <?php if(isset($keyword)){ ?>
   <div class="row-fluid">
    <div class="span6">
     <div class="control-group">
      <div class="controls">
       Data Dicari : <b><label style="font-weight: bold;"><?php echo $keyword ?></label></b>
      </div>
     </div>
    </div>
   </div>
   <?php } ?>
   <div class="widget-box">
    <div class="widget-title">
     <span class="icon"><i class="icon-th"></i></span> 
     <h5>Daftar Buku</h5>
    </div>
    <div class="widget-content nopadding data" style="overflow: auto;max-height: 400px;" id="data_buku">
     <table class="table table-bordered data-table">
      <thead>
       <tr>
        <th>No</th>
        <th>No Induk Buku</th>
        <th>Nama</th>
        <th>Kategori</th>
        <th>Rak</th>
        <th>Judul</th>
        <th>Pengarang</th>
        <th>Harga</th>
        <th>Keterangan</th>
        <th>Harga Denda Buku</th>
        <th>Stock</th>
        <th>Buku Dipinjam</th>
        <th>Buku Hilang</th>
        <th>Action</th>
       </tr>
      </thead>
      <tbody>
       <?php if (!empty($data)) { ?>
        <?php $no = $pagination['last_no'] + 1; ?>
        <?php foreach ($data as $value) { ?>
         <tr class="text-center">
          <td class="center"><?php echo $no++ ?></td>
          <td class="center"><?php echo $value['no_induk_buku'] == '' ? '-' : $value['no_induk_buku'] ?></td>
          <td class="center"><?php echo $value['nama'] ?></td>
          <td class="center"><?php echo $value['kategori'] ?></td>
          <td class="center"><?php echo $value['rak'] ?></td>
          <td class="center"><?php echo $value['judul'] ?></td>
          <td class="center"><?php echo $value['pengarang'] ?></td>
          <td class="center"><?php echo number_format($value['harga'], 2, ',', '.') ?></td>
          <td class="center"><?php echo $value['keterangan'] ?></td>
          <td class="center"><?php echo number_format($value['denda'], 2, ',', '.') ?></td>
          <td class="center"><?php echo $value['stock'] ?></td>
          <td class="center"><?php echo $value['buku_pinjam'] == '' ? 0 : $value['buku_pinjam'] ?></td>
          <td class="center"><?php echo $value['buku_hilang'] == '' ? 0 : $value['buku_hilang'] ?></td>
          <td class="center">
           <i class="mdi mdi-pencil-circle mdi-24px" onclick="buku.edit('<?php echo $value['id'] ?>')"></i>
           <i class="mdi mdi-delete mdi-24px" onclick="buku.konfirmasi_delete('<?php echo $value['id'] ?>', '<?php echo $value['nama'] ?>')"></i>
           <?php if ($value['no_induk_buku'] != '') { ?>
            <i class="mdi mdi-barcode mdi-24px" onclick="buku.showBarcode('<?php echo $value['no_induk_buku'] ?>')"></i>
           <?php } ?>
          </td>
         </tr>
        <?php } ?>
       <?php } else { ?>
        <tr class="text-center">
         <td class="center" colspan="13">Tidak Ada Data</td>
        </tr>
       <?php } ?>
      </tbody>
     </table>
    </div>
   </div>

   <div class="pagination alternate text-right">
    <?php if (isset($pagination)) { ?>    
     <?php echo $pagination['links'] ?>
    <?php } ?>
   </div>
  </div>
 </div>  
</div>