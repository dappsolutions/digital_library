<table class="table table-bordered data-table">
 <thead>
  <tr>
   <th>No</th>
   <th>No Induk Buku</th>
   <th>Nama</th>
   <th>Kategori</th>
   <th>Rak</th>
   <th>Judul</th>
   <th>Pengarang</th>
   <th>Harga</th>
   <th>Keterangan</th>
   <th>Harga Denda Buku</th>
   <th>Stock</th>
   <th>Buku Dipinjam</th>
   <th>Buku Hilang</th>
   <th>Action</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($data)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($data as $value) { ?>
    <tr class="text-center">
     <td class="center"><?php echo $no++ ?></td>
     <td class="center"><?php echo $value['no_induk_buku'] == '' ? '-' : $value['no_induk_buku'] ?></td>
     <td class="center"><?php echo $value['nama'] ?></td>
     <td class="center"><?php echo $value['kategori'] ?></td>
     <td class="center"><?php echo $value['rak'] ?></td>
     <td class="center"><?php echo $value['judul'] ?></td>
     <td class="center"><?php echo $value['pengarang'] ?></td>
     <td class="center"><?php echo number_format($value['harga'], 2, ',', '.') ?></td>
     <td class="center"><?php echo $value['keterangan'] ?></td>
     <td class="center"><?php echo number_format($value['denda'], 2, ',', '.') ?></td>
     <td class="center"><?php echo $value['stock'] ?></td>
     <td class="center"><?php echo $value['buku_pinjam'] == '' ? 0 : $value['buku_pinjam'] ?></td>
     <td class="center"><?php echo $value['buku_hilang'] == '' ? 0 : $value['buku_hilang'] ?></td>
     <td class="center">
      <i class="mdi mdi-pencil-circle mdi-24px" onclick="buku.edit('<?php echo $value['id'] ?>')"></i>
      <i class="mdi mdi-delete mdi-24px" onclick="buku.konfirmasi_delete('<?php echo $value['id'] ?>', '<?php echo $value['nama'] ?>')"></i>
      <i class="mdi mdi-barcode mdi-24px" onclick="buku.showBarcode('<?php echo $value['nama'] ?>')"></i>
     </td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr class="text-center">
    <td class="center" colspan="13">Tidak Ada Data</td>
   </tr>
  <?php } ?>
 </tbody>
</table>