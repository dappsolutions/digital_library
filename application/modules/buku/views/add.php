<div class="container-fluid">
 <div class="row-fluid">
  <div class="span12">
   <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
     <h5>Tambah Buku</h5>
    </div>
    <div class="widget-content nopadding">
     <form action="#" method="get" class="form-horizontal">
      <div class="control-group">
       <label class="control-label">No. Induk Buku</label>
       <div class="controls">
        <input type="text" id="no_induk_buku" class="span11" placeholder="No. Induk Buku" />
       </div>
      </div>
      
      <div class="control-group">
       <label class="control-label">Nama Buku</label>
       <div class="controls">
        <input type="text" id="nama" error="Nama Buku" class="span11 required" placeholder="Nama Buku" />
       </div>
      </div>
      
      <div class="control-group">
       <label class="control-label">Kategori</label>
       <div class="controls">
        <select class="form-control" id="kategori">
         <?php foreach ($list_kategori as $v_kat) { ?>
          <option value="<?php echo $v_kat['id'] ?>"><?php echo $v_kat['kategori'] ?></option>
         <?php } ?>
        </select>
       </div>
      </div>
      <div class="control-group">
       <label class="control-label">Rak</label>
       <div class="controls">
        <select class="form-control" id="rak">
         <?php foreach ($list_rak as $v_rak) { ?>
          <option value="<?php echo $v_rak['id'] ?>"><?php echo $v_rak['rak'] ?></option>
         <?php } ?>
        </select>
       </div>
      </div>
      <div class="control-group">
       <label class="control-label">Judul Buku</label>
       <div class="controls">
        <input type="text" id="judul" error="Judul Buku" class="span11 required" placeholder="Judul Buku" />
       </div>
      </div>
      
      <div class="control-group">
       <label class="control-label">Pengarang Buku</label>
       <div class="controls">
        <input type="text" id="pengarang" error="Pengarang Buku" class="span11 " placeholder="Pengarang Buku" />
       </div>
      </div>
      
      <div class="control-group">
       <label class="control-label">Stock Buku</label>
       <div class="controls">
        <input type="text" id="stock" error="Stock Buku" class="span11 " placeholder="Stock Buku" />
       </div>
      </div>
      
      <div class="control-group">
       <label class="control-label">Harga Buku</label>
       <div class="controls">
        <input type="text" id="harga" error="Harga Buku" class="span11 required" placeholder="Harga Buku" />
       </div>
      </div>
      <div class="control-group">
       <label class="control-label">Keterangan</label>
       <div class="controls">
        <textarea name="" id="keterangan"></textarea>
       </div>
      </div>
      <div class="control-group">
       <label class="control-label">Denda Buku</label>
       <div class="controls">
        <input type="text" id="denda" error="Denda Buku" class="span11 required" placeholder="Denda Buku" />
       </div>
      </div>
      <div class="form-actions">
       <button type="submit" class="btn btn-success" onclick="buku.save()">Save</button>
      </div>
     </form>
    </div>
   </div>
  </div>
 </div>
 <hr>
</div>