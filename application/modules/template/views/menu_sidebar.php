<div id="sidebar"><a href="<?php echo base_url().'home' ?>" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a><ul>
  <li class="active"><a href="<?php echo base_url().'home' ?>"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li> </li>
  <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Master</span></a>
   <ul>
    <li><a href="<?php echo base_url().'siswa' ?>">Siswa</a></li>
    <li><a href="<?php echo base_url().'buku' ?>">Buku</a></li>
    <li><a href="<?php echo base_url().'kategori' ?>">Kategori</a></li>
    <li><a href="<?php echo base_url().'rak' ?>">Rak</a></li>
    <li><a href="<?php echo base_url().'range_peminjaman' ?>">Range Peminjaman</a></li>
   </ul>
  </li>
  <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Transaksi</span></a>
   <ul>
    <li><a href="<?php echo base_url().'peminjaman' ?>">Peminjaman</a></li>
    <li><a href="<?php echo base_url().'pengembalian' ?>">Pengembalian</a></li>
    <li><a href="<?php echo base_url().'pembayaran_denda' ?>">Pembayaran Denda</a></li>
   </ul>
  </li>
  <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Data</span></a>
   <ul>
    <li><a href="<?php echo base_url().'buku_hilang' ?>">Buku Hilang</a></li>
    <li><a href="<?php echo base_url().'buku_pinjam' ?>">Buku Dipinjam</a></li>
   </ul>
  </li>
  <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Absensi</span></a>
   <ul>
    <li><a href="<?php echo base_url().'absensi_siswa' ?>">Absensi Siswa</a></li>
   </ul>
  </li>
 </ul>
</div>