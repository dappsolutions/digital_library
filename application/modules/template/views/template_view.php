<!DOCTYPE html>
<html lang="en">
 <?php echo Modules::run('header'); ?>
 <body>
  <?php echo $this->load->view('header', array(), true); ?>
  <?php echo $this->load->view('top_header', array(), true); ?>
  <?php echo $this->load->view('menu_sidebar', array(), true); ?>
  <div id="content">
   <?php echo $this->load->view($module . '/' . $view); ?>
   <?php // echo $this->load->view('content', array()); ?>
  </div>
  <?php echo $this->load->view('footer', array(), true); ?>
 </body>
</html>
