<?php

class Home extends MX_Controller{
 public function __construct() {
  parent::__construct();
  if($this->session->userdata('user') == ""){
   redirect(base_url());
  }
 }
 
 
 public function get_module() {
  return "home";
 }

 public function index() {
  $data['module'] = $this->get_module();
  $data['jumlah_buku_dipinjam'] = count(Modules::run('buku_pinjam/getDataBuku'));
  $data['jumlah_buku_hilang'] = count(Modules::run('buku_hilang/getDataBuku'));
  $data['jumlah_peminjam'] = count(Modules::run('peminjaman/getDatapeminjaman'));
  $data['jumlah_pengembali'] = count(Modules::run('pengembalian/getDatapeminjaman'));
  $data['jumlah_siswa'] = count($this->getJumlahSiswa());
  $data['jumlah_buku'] = count($this->getJumlahBuku());
  $data['jumlah_rak'] = count($this->getJumlahRak());
  $data['view'] = 'index';
  
  $data['value_dashboard'] = $this->getDataDashboard();
  echo Modules::run('template', $data);
 }
 
 public function getJumlahSiswa() {
  $data = Modules::run('database/get', array(
    'table' => 'siswa'
  ));
  
  $result = array();
  if(!empty($data)){
   $result = $data->result_array();
  }
  
  return $result;
 }
 
 public function getJumlahBuku() {
  $data = Modules::run('database/get', array(
    'table' => 'buku',
    'limit'=> 1000000
  ));
  
  $result = array();
  if(!empty($data)){
   $result = $data->result_array();
  }
  
  return $result;
 }
 
 public function getJumlahRak() {
  $data = Modules::run('database/get', array(
    'table' => 'rak'
  ));
  
  $result = array();
  if(!empty($data)){
   $result = $data->result_array();
  }
  
  return $result;
 }
 
 public function getDataDashboard() {
  $value = array();      
  
  //januari
  $januari = $this->getDataPeminjaman(date('Y').'-01');
  $count = 0;
  if(!empty($januari)){ 
   $count = count($januari);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Jan',
   'color' => "#57a532",
   'value' => $count
  ));
  
 //Februari
  $februari = $this->getDataPeminjaman(date('Y').'-02');
  $count = 0;
  if(!empty($februari)){ 
   $count = count($februari);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Feb',
   'color' => "#57a532",
   'value' => $count
  ));
 
//Maret
  $maret = $this->getDataPeminjaman(date('Y').'-03');
  $count = 0;
  if(!empty($maret)){ 
   $count = count($maret);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Mar',
   'color' => "#57a532",
   'value' => $count
  ));

 //April
  $april = $this->getDataPeminjaman(date('Y').'-04');
  $count = 0;
  if(!empty($april)){ 
   $count = count($april);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Apr',
   'color' => "#57a532",
   'value' => $count
  ));
 
//Mei
  $mei = $this->getDataPeminjaman(date('Y').'-05');
  $count = 0;
  if(!empty($mei)){ 
   $count = count($mei);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Mei',
   'color' => "#57a532",
   'value' => $count
  ));

 //Juni
  $juni = $this->getDataPeminjaman(date('Y').'-06');
  $count = 0;
  if(!empty($juni)){ 
   $count = count($juni);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Jun',
   'color' => "#57a532",
   'value' => $count
  ));
 
 //Juli
  $juli = $this->getDataPeminjaman(date('Y').'-07');
  $count = 0;
  if(!empty($juli)){ 
   $count = count($juli);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Jul',
   'color' => "#57a532",
   'value' => $count
  ));
 
 //Agustus
  $agustus = $this->getDataPeminjaman(date('Y').'-08');
  $count = 0;
  if(!empty($agustus)){ 
   $count = count($agustus);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Agu',
   'color' => "#57a532",
   'value' => $count
  ));
 
 //September
  $september = $this->getDataPeminjaman(date('Y').'-09');
  $count = 0;
  if(!empty($september)){ 
   $count = count($september);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Sep',
   'color' => "#57a532",
   'value' => $count
  ));
 
//Oktober
  $oktober= $this->getDataPeminjaman(date('Y').'-10');
  $count = 0;
  if(!empty($oktober)){ 
   $count = count($oktober);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Okt',
   'color' => "#57a532",
   'value' => $count
  ));

 //November
  $november = $this->getDataPeminjaman(date('Y').'-11');
  $count = 0;
  if(!empty($november)){ 
   $count = count($november);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Nov',
   'color' => "#57a532",
   'value' => $count
  ));
 
//Desember
  $desember = $this->getDataPeminjaman(date('Y').'-12');
  $count = 0;
  if(!empty($desember)){ 
   $count = count($desember);
  }else{   
   $count = 0;
  }
  
  array_push($value, array(
   'label' => 'Des',
   'color' => "#57a532",
   'value' => $count
  ));
  
  return json_encode($value);  
 }
 
 public function getDataPeminjaman($date) {
  $data = Modules::run('database/get', array(
    'table' => 'transaksi_peminjaman tp',
    'field' => array('tp.id', 's.nama as siswa', 's.nis', 'tp.tanggal_pinjam', 'rp.jumlah_hari'),
    'join' => array(
     array('siswa s', 'tp.siswa = s.id'),
     array('range_peminjaman rp', 'tp.range_peminjaman = rp.id'),
    ),
    'like'=> array(
     array('tp.tanggal_pinjam', $date)
    )
  ));
  
  $result = array();
  if(!empty($data)){
   $result = $data->result_array();
  }
  
  return $result;
 }
}

