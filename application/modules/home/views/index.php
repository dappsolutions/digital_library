<div id="content-header">
 <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a></div>
</div>
<div class="container-fluid">
 <div class="quick-actions_homepage">
  <ul class="quick-actions">
   <li> <a href="<?php echo base_url().'buku_pinjam' ?>"> <i class="icon-book"></i> Buku Dipinjam 
    <?php echo $jumlah_buku_dipinjam > 0 ? '('.$jumlah_buku_dipinjam.')' : '' ?></a> </li>
   <li> <a href="<?php echo base_url().'buku_hilang' ?>"> <i class="icon-book"></i> Buku Hilang 
    <?php echo $jumlah_buku_hilang > 0 ? '('.$jumlah_buku_hilang.')' : '' ?></a> </li>
   <li> <a href="<?php echo base_url().'peminjaman' ?>"> <i class="icon-people"></i> Peminjaman Buku 
    <?php echo $jumlah_peminjam > 0 ? '('.$jumlah_peminjam.')' : '' ?></a> </li>
   <li> <a href="<?php echo base_url().'pengembalian' ?>"> <i class="icon-people"></i> Pengembalian Buku 
    <?php echo $jumlah_pengembali > 0 ? '('.$jumlah_pengembali.')' : '' ?></a> </li>
  </ul>
 </div>

 <div class="row-fluid">
  <div class="widget-box">
   <div class="widget-title"><span class="icon"><i class="icon-tasks"></i></span>
    <h5>Site Analytics</h5>
    <div class="buttons"><a href="#" class="btn btn-mini btn-success"><i class="icon-refresh"></i> Update status</a></div>
   </div>
   <div class="widget-content">
    <div class="row-fluid">
     <div class="span8">
      <div id="value_dashboard" class="hide">
       <?php  
        echo $value_dashboard
       ?>
      </div>
      <!--<div class="chart"></div>-->
      <div id="chart-container"></div>
     </div>
     <div class="span4">
      <ul class="stat-boxes2">
       <li>
        <div class="left"><span><span style="display: none;">2,4,9,7,12,10,12</span>
          <canvas width="50" height="24"></canvas>
         </span></div>
        <div class="right"> <strong><?php echo $jumlah_siswa ?></strong> Siswa </div>
       </li>
       <li>
        <div class="left"><span><span style="display: none;">10,15,8,14,13,10,10,15</span>
          <canvas width="50" height="24"></canvas>
         </span></div>
        <div class="right"> <strong><?php echo $jumlah_buku ?></strong> Buku </div>
       </li>
       <li>
        <div class="left"><span><span style="display: none;">3,5,6,16,8,10,6</span>
          <canvas width="50" height="24"></canvas>
         </span></div>
        <div class="right"> <strong><?php echo $jumlah_rak ?></strong> Rak</div>
       </li>
      </ul>
     </div>
    </div>
   </div>
  </div>
 </div>
 <hr>
</div>