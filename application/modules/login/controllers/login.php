<?php

class Login extends MX_Controller {

 public function index() {
  echo $this->load->view('index', array(), true);
 }

 public function sign_in() {
  $username = $this->input->post('username');
  $password = $this->input->post('password');

  $data = Modules::run('database/get', array(
  'table' => 'user',
  'where' => array('username' => $username, 'password' => $password)
  ));

  $is_valid = false;
  if (!empty($data)) {
   $data = $data->row_array();
   $this->session->set_userdata(array(
    'user'=> $data['id'],
    'name'=> $data['username']
   ));
   
   $is_valid = true;
  }

  echo json_encode(array('is_valid'=> $is_valid));
 }

 public function logout() {
  $this->session->sess_destroy();
  redirect(base_url());
 }

}
