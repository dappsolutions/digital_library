<!DOCTYPE html>
<html lang="en"> 
 <head>
  <title>Login Admin</title><meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="<?php echo base_url() ?>files/css/bootstrap.min.css" />
		<link rel="stylesheet" href="<?php echo base_url() ?>files/css/bootstrap-responsive.min.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>files/css/maruti-login.css" />
  <link rel="stylesheet" href="<?php echo base_url() ?>files/css/toastr.min.css" />
    
  <script src="<?php echo base_url() ?>files/js/controllers/login.js"></script> 
  <script src="<?php echo base_url() ?>files/js/jquery.js"></script> 
  <script src="<?php echo base_url() ?>files/js/toastr.min.js"></script> 
  <script src="<?php echo base_url() ?>files/js/bootbox.js"></script> 
 </head>
 <body>
  <div id="loginbox">            
   <form id="loginform" class="form-vertical" action="index.html">
    <div class="control-group normal_text"> <h3>Login</h3></div>
    <div class="control-group">
     <div class="controls">
      <div class="main_input_box">
       <span class="add-on"><i class="icon-user"></i></span><input type="text" id="username" placeholder="Username" />
      </div>
     </div>
    </div>
    <div class="control-group">
     <div class="controls">
      <div class="main_input_box">
       <span class="add-on"><i class="icon-lock"></i></span><input type="password" id='password' placeholder="Password" />
      </div>
     </div>
    </div>
    <div class="form-actions">
     <span class="pull-right"><input type="button" onclick="login.sign_in()" class="btn btn-success" value="Login" /></span>
    </div>
   </form>
   <form id="recoverform" action="#" class="form-vertical">
				<p class="normal_text">Enter your e-mail address below and we will send you instructions how to recover a password.</p>

    <div class="controls">
     <div class="main_input_box">
      <span class="add-on"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
     </div>
    </div>

    <div class="form-actions">
     <span class="pull-left"><a href="#" class="flip-link btn btn-inverse" id="to-login">&laquo; Back to login</a></span>
     <span class="pull-right"><input type="submit" class="btn btn-info" value="Recover" /></span>
    </div>
   </form>
  </div>

  <script src="<?php echo base_url() ?>files/js/jquery.min.js"></script>  
  <script src="<?php echo base_url() ?>files/js/maruti.login.js"></script> 
 </body>

</html>
