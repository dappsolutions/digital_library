<table class="table table-bordered data-table">
 <thead>
  <tr>
   <th>No</th>
   <th>Nis</th>
   <th>Siswa</th>
   <th>Tanggal Masuk</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($data)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($data as $value) { ?>
    <tr style="">
     <td><?php echo $no++ ?></td>
     <td><?php echo $value['nis'] ?></td>
     <td><?php echo $value['siswa'] ?></td>
     <td><?php echo date('d M Y H:i:s', strtotime($value['tanggal_masuk'])) ?></td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td colspan="5">Tidak Ada Absensi Siswa</td>
   </tr>
  <?php } ?>
 </tbody>
</table>