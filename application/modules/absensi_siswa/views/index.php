<script src="<?php echo base_url() ?>files/js/controllers/absensi_siswa.js"></script> 
<div id="content-header">
 <div id="breadcrumb"> <a href="<?php echo base_url() . 'home' ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
  <a href="<?php echo base_url() . 'absensi_siswa' ?>" class="current">Absensi Siswa</a> </div>
 <h1>Absensi Siswa</h1>
</div>
<div class="container-fluid">
 <div class="row-fluid">
  <div class="" style="">
   <div class="span5">
    <div class="widget-box">
     <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5>Form Absensi Siswa</h5>
     </div>
     <div class="widget-content nopadding">
      <form action="#" method="get" class="form-horizontal">
       <div class="control-group">
        <label class="control-label">NIS</label>
        <div class="controls">
         <input type="text" id="nis" error="NIS" class="span11 required" placeholder="NIS" onkeypress="absensi_siswa.doAbsensi(this, event)"/>
        </div>
       </div>
       <div class="list_buku">

       </div>       
       <div class="form-actions">
        <button type="button" class="btn btn-success" onclick="absensi_siswa.save()">Proses</button>
       </div>
      </form>
     </div>
    </div>
   </div>


   <div class="span7">
    <div class="widget-box">
     <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5>Daftar Absensi Siswa</h5>
     </div>
     <div class="widget-content nopadding">
      <div class="" style="padding: 16px;">
       <div class="control-group">
        <label class="control-label">NIS</label>
        <div class="controls">
         <input type="text" class="span11 " placeholder="Pencarian" style="width: 70%;" onkeypress="absensi_siswa.search(this, event)"/>
        </div>
       </div>
       <div class="control-group">
        <!--<label class="control-label">Tanggal Pinjam</label>-->
        <div class="controls" style="max-height: 400px;overflow: auto;">
         <div class=""  id="data_siswa_masuk">
          <table class="table table-bordered data-table">
           <thead>
            <tr>
             <th>No</th>
             <th>Nis</th>
             <th>Siswa</th>
             <th>Tanggal Masuk</th>
            </tr>
           </thead>
           <tbody>
            <?php if (!empty($data)) { ?>
             <?php $no = 1; ?>
             <?php foreach ($data as $value) { ?>
              <tr style="">
               <td><?php echo $no++ ?></td>
               <td><?php echo $value['nis'] ?></td>
               <td><?php echo $value['siswa'] ?></td>
               <td><?php echo date('d M Y H:i:s', strtotime($value['tanggal_masuk'])) ?></td>
              </tr>
             <?php } ?>
            <?php } else { ?>
             <tr>
              <td colspan="5">Tidak Ada Absensi Siswa</td>
             </tr>
            <?php } ?>
           </tbody>
          </table>
         </div>         
        </div>
       </div>
      </div>             
     </div>
    </div>
    <br/>
    <hr/>
   </div>
  </div>
 </div><hr>
</div>