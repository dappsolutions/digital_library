<?php

class Absensi_siswa extends MX_Controller {

 public function __construct() {
  parent::__construct();
  if ($this->session->userdata('user') == "") {
   redirect(base_url());
  }
 }

 public function get_module() {
  return "absensi_siswa";
 }

 public function get_table_name() {
  return 'absensi_siswa';
 }

 public function index() {
  $data['module'] = $this->get_module();
  $data['data'] = $this->getDataabsensi_siswa();
  $data['view'] = 'index';
  echo Modules::run('template', $data);
 }

 public function getDataabsensi_siswa() {
  $data = Modules::run('database/get', array(
  'table' => $this->get_table_name() . ' abs',
  'field' => array('abs.id', 's.nis', 's.nama as siswa',
  'evt.stamp as tanggal_masuk'),
  'join' => array(
  array('siswa s', 'abs.siswa = s.id'),
  array("change_log cl", "abs.id = cl.primary_key and cl.tabel = 'absensi_siswa'"),
  array('event evt', 'cl.event = evt.id'),
  ),
  'orderby'=> 'abs.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function search() {
  $keyword = rawurldecode($this->input->post('keyword'));

  $data = Modules::run('database/get', array(
  'table' => $this->get_table_name() . ' abs',
  'field' => array('abs.id', 's.nis', 's.nama as siswa',
  'evt.stamp as tanggal_masuk'),
  'join' => array(
  array('siswa s', 'abs.siswa = s.id'),
  array("change_log cl", "abs.id = cl.primary_key and cl.tabel = 'absensi_siswa'"),
  array('event evt', 'cl.event = evt.id'),
  ),
  'is_or_like' => true,
  'like' => array(
  array('abs.id', $keyword),
  array('s.nama', $keyword),
  array('s.nis', $keyword),
  array('evt.stamp', $keyword)
  )
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['data'] = $result;
  echo $this->load->view('search', $content, true);
 }

 public function isExistSiswa($nis) {
  $data = Modules::run('database/get', array(
  'table' => 'siswa',
  'where' => array('nis' => $nis)
  ));

  $is_exist = false;
  if (!empty($data)) {
   $is_exist = true;
  }

  return $is_exist;
 }

 public function doAbsensi($nis) {
  $is_exist_siswa = $this->isExistSiswa($nis);

  $is_valid = false;
  if ($is_exist_siswa) {
   $this->db->trans_begin();
   try {
    $event = Modules::run('helper/_insert_event', 'Tambah Absensi');
    $data_siswa = $this->get_post_data($nis);
    $absensi_siswa = Modules::run('database/_insert', $this->get_table_name(), $data_siswa);
    Modules::run('helper/_insert_change_log', $this->get_table_name(), $absensi_siswa, $event);
    $this->db->trans_commit();
    $is_valid = true;
   } catch (Exception $ex) {
    $this->db->trans_rollback();
   }
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function get_post_data($nis) {
  $data['siswa'] = $this->getIdSiswa($nis);
  return $data;
 }

 public function getIdSiswa($nis) {
  $data = Modules::run('database/get', array(
  'table' => 'siswa',
  'where' => array('nis' => $nis)
  ));

  $id = '';
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  }

  return $id;
 }

}
