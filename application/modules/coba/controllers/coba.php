<?php

class Coba extends MX_Controller{
 public function get_module() {
  return 'coba';
 }
 
 public function index() {
  $data['module'] = $this->get_module();
  $data['view'] = 'coba_view';
  
  echo Modules::run('template', $data);
 }
}

