<?php

class Helper extends MX_Controller{
 public function _insert_event($message) {
  $event = Modules::run('database/_insert', 'event', array(
   'stamp'=> date('Y-m-d H:i:s'),
   'user'=> $this->session->userdata('user'),
   'message'=> $message
  ));
  
  return $event;
 }
 
 public function _insert_change_log($table, $primary_key, $event) {
  Modules::run('database/_insert', 'change_log', array(
   'tabel'=> $table,
   'primary_key'=> $primary_key,
   'event'=> $event
  ));
 }
}

