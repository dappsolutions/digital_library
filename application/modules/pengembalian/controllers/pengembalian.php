<?php

class Pengembalian extends MX_Controller {

 public function __construct() {
  parent::__construct();
  if ($this->session->userdata('user') == "") {
   redirect(base_url());
  }
 }

 public function get_module() {
  return "pengembalian";
 }

 public function get_table_name() {
  return 'transaksi_peminjaman';
 }

 public function index() {
  $data['module'] = $this->get_module();
  $data['data'] = $this->getDatapeminjaman();
  $data['view'] = 'index';
  echo Modules::run('template', $data);
 }

 public function getDatapeminjaman() {
  $data = Modules::run('database/get', array(
  'table' => $this->get_table_name() . ' tp',
  'field' => array('tp.id', 's.nama as siswa', 's.nis', 'tp.tanggal_pinjam',
  'tp.tanggal_kembali', 'td.tanggal_bayar as tgl_bayar_denda_pinjam',
  'tdbh.tanggal_bayar as tgl_bayar_denda_buku_hilang',
  'tdbh.id as tdbh_id', 'td.id as td_id'),
  'join' => array(
  array('siswa s', 'tp.siswa = s.id'),
  array('transaksi_denda td', 'tp.id = td.transaksi_peminjaman', 'left'),
  array('transaksi_denda_buku_hilang tdbh', 'tp.id = tdbh.transaksi_peminjaman', 'left'),
  ),
  'where' => 'tp.tanggal_kembali is not null',
  'orderby' => 'tp.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function search() {
  $keyword = rawurldecode($this->input->post('keyword'));
  $data = Modules::run('database/get', array(
  'table' => $this->get_table_name() . ' tp',
  'field' => array('tp.id', 's.nama as siswa', 's.nis', 'tp.tanggal_pinjam',
  'tp.tanggal_kembali', 'td.tanggal_bayar as tgl_bayar_denda_pinjam',
  'tdbh.tanggal_bayar as tgl_bayar_denda_buku_hilang',
  'tdbh.id as tdbh_id', 'td.id as td_id'),
  'join' => array(
  array('siswa s', 'tp.siswa = s.id'),
  array('transaksi_denda td', 'tp.id = td.transaksi_peminjaman', 'left'),
  array('transaksi_denda_buku_hilang tdbh', 'tp.id = tdbh.transaksi_peminjaman', 'left'),
  ),
  'inside_brackets' => true,
  'is_or_like' => true,
  'like' => array(
  array('tp.id', $keyword),
  array('s.nama', $keyword),
  array('s.nis', $keyword),
  array('tp.tanggal_pinjam', $keyword),
  array('tp.tanggal_kembali', $keyword),
  ),
  'where' => 'tp.tanggal_kembali is not null',
  'orderby' => 'tp.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  $content['data'] = $result;
  echo $this->load->view('search', $content, true);
 }

 public function getAutocompleteSiswaPinjam($nis) {
  $data = Modules::run('database/get', array(
  'table' => $this->get_table_name() . ' tp',
  'field' => array('tp.id', 's.nama as siswa', 's.nis', 'tp.tanggal_pinjam',
  'tp.tanggal_kembali', 'buk.nama as buku', 'buk.id as buku_id'),
  'join' => array(
  array('transaksi_peminjaman_has_buku tphb', 'tp.id = tphb.' . $this->get_table_name()),
  array('siswa s', 'tp.siswa = s.id'),
  array('buku buk', 'tphb.buku = buk.id'),
  ),
  'where' => "tp.tanggal_kembali is null and s.nis = '" . $nis . "'"
  ));

  $is_valid = false;
  $view = "Tidak ada Transaksi Peminjaman oleh Siswa Tersebut";
  if (!empty($data)) {
   $content['data'] = $data->result_array();
   $is_valid = true;
   $view = $this->load->view('getListBuku', $content, true);
  }

  echo json_encode(array('is_valid' => $is_valid, 'view' => $view));
 }

 public function get_post_data($value) {
  $data['peminjaman'] = $value->peminjaman;

  return $data;
 }

 public function getIdSiswa($nis) {
  $data = Modules::run('database/get', array(
  'table' => 'siswa',
  'where' => array('nis' => $nis)
  ));

  $id = '';
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  }

  return $id;
 }

 public function getIdBuku($buku) {
  $data = Modules::run('database/get', array(
  'table' => 'buku',
  'where' => array('nama' => $buku)
  ));

  $id = '';
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  }

  return $id;
 }

 public function get_post_data_transaksi($value) {
  $data['siswa'] = $this->getIdSiswa($value->nis);
  $data['tanggal_pinjam'] = date('Y-m-d');

  return $data;
 }

 public function get_post_data_transaksi_has_buku($value, $transaksi_peminjaman) {
  $data['buku'] = $this->getIdBuku($value->buku);
  $data['transaksi_peminjaman'] = $transaksi_peminjaman;

  return $data;
 }

 public function InsertTransaksiDenda($tanggal_pinjam, $buku, $transaksi_peminjaman, $event) {
  $date_now = new DateTime(date('Y-m-d'));
  $date_pinjam = new DateTime($tanggal_pinjam);
  $range_borrow_day = $date_now->diff($date_pinjam);
  $day_pinjam = intval($range_borrow_day->days);
  $range_peminjaman = $this->getRangePeminjaman($transaksi_peminjaman);

  $denda = 0;
  if ($day_pinjam > $range_peminjaman) {
   foreach ($buku as $value) {
    if ($value->hilang != 1) {
     $price_denda_buku = $this->getPriceDendaBuku($value->buku);
     $price_denda = $price_denda_buku * $day_pinjam;
     $denda += $price_denda;
    }
   }
  }

  if ($denda > 0) {
   $transaksi_denda = Modules::run('database/_insert', 'transaksi_denda', array(
   'transaksi_peminjaman' => $transaksi_peminjaman,
   'denda' => $denda
   ));
   Modules::run('helper/_insert_change_log', 'transaksi_denda', $transaksi_denda, $event);
  }
 }

 public function getRangePeminjaman($transaksi_peminjaman) {
  $data = Modules::run('database/get', array(
  'table' => 'transaksi_peminjaman tp',
  'field' => array('rp.jumlah_hari'),
  'join' => array(
  array('range_peminjaman rp', 'tp.range_peminjaman = rp.id'),
  ),
  'where' => array('tp.id' => $transaksi_peminjaman)
  ))->row_array();

  return $data['jumlah_hari'];
 }

 public function InsertTransaksiDendaBukuHilang($buku, $transaksi_peminjaman, $event) {
  $denda = 0;
  foreach ($buku as $value) {
   if ($value->hilang == 1) {
    $price_buku = $this->getPriceBuku($value->buku);
    $denda += $price_buku;
   }
  }

  if ($denda > 0) {
   $transaksi_denda_buku_hilang = Modules::run('database/_insert', 'transaksi_denda_buku_hilang', array(
   'transaksi_peminjaman' => $transaksi_peminjaman,
   'denda' => $denda
   ));
   Modules::run('helper/_insert_change_log', 'transaksi_denda_buku_hilang', $transaksi_denda_buku_hilang, $event);
  }
 }

 public function getPriceBuku($buku_id) {
  $data = Modules::run('database/get', array(
  'table' => 'buku',
  'where' => array('id' => $buku_id)
  ))->row_array();

  return $data['harga'];
 }

 public function getPriceDendaBuku($buku_id) {
  $data = Modules::run('database/get', array(
  'table' => 'buku',
  'where' => array('id' => $buku_id)
  ))->row_array();

  return $data['denda'];
 }

 public function getDataBuku($id) {
  $data = Modules::run('database/get', array(
  'table' => 'buku',
  'where' => array('id' => $id)
  ));

  return $data->row_array();
 }

 public function save() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Tambah Pengembalian');

   Modules::run('database/_update', 'transaksi_peminjaman', array('tanggal_kembali' => date('Y-m-d')), array('id' => $data->transaksi_peminjaman));
   Modules::run('helper/_insert_change_log', 'transaksi_peminjaman', $data->transaksi_peminjaman, $event);

   $this->InsertTransaksiDenda($data->tanggal_pinjam, $data->buku, $data->transaksi_peminjaman, $event);
   $this->InsertTransaksiDendaBukuHilang($data->buku, $data->transaksi_peminjaman, $event);

   foreach ($data->buku as $value) {
    //insert ke list buku_hilang jika hilang
    $data_buku = $this->getDataBuku($value->buku);
    if ($value->hilang == 1) {
     $buku_hilang = Modules::run('database/_insert', 'buku_hilang', array(
     'buku' => $value->buku
     ));
     $stok_buku = $data_buku['stock'] - 1;
     $stok_buku_hilang = $data_buku['buku_hilang'] == '' ? 0 : $data_buku['buku_hilang'];
     Modules::run('helper/_insert_change_log', 'buku_hilang', $buku_hilang, $event);
     $this->db->query('delete from buku_pinjam where buku = ' . $value->buku . ' limit 1');
     Modules::run('database/_update', 'buku', array(
     'stock' => $stok_buku,
     'buku_hilang' => ($stok_buku_hilang +1)
     ), array('id' => $value->buku));
    } else {
     $this->db->query('delete from buku_pinjam where buku = ' . $value->buku . ' limit 1');
     $buku_keluar = $data_buku['buku_keluar'] == '' ? 0 : $data_buku['buku_keluar'];
     $stok_buku = $data_buku['stock'] + 1;
     Modules::run('database/_update', 'buku', array(
     'buku_keluar' => ($buku_keluar - 1),
     'stock' => $stok_buku
     ), array('id' => $value->buku));
    }
    Modules::run('helper/_insert_change_log', 'buku', $value->buku, $event);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function detailPeminjaman($id, $is_hilang = '') {
  $data = Modules::run('database/get', array(
  'table' => $this->get_table_name() . ' tp',
  'field' => array('tp.tanggal_pinjam', 's.nama as siswa', 's.nis',
  'tp.tanggal_kembali', 'td.denda as denda_peminjaman',
  'tdbh.denda as denda_buku_hilang'),
  'join' => array(
  array('siswa s', 'tp.siswa = s.id'),
  array('transaksi_denda td', 'tp.id = td.transaksi_peminjaman', 'left'),
  array('transaksi_denda_buku_hilang tdbh', 'tp.id = tdbh.transaksi_peminjaman', 'left'),
  ),
  'where' => array('tp.id' => $id)
  ))->row_array();
  $data['list_buku'] = $this->getListBukuDipinjam($id);
  $data['is_hilang'] = $is_hilang;
  echo $this->load->view('detailPeminjaman', $data, true);
 }

 public function getListBukuDipinjam($id) {
  $data = Modules::run('database/get', array(
  'table' => 'transaksi_peminjaman_has_buku tphb',
  'field' => array('buk.nama as buku', 'bh.id as buku_hilang'),
  'join' => array(
  array('buku buk', 'tphb.buku = buk.id'),
  array('buku_hilang bh', 'buk.id = bh.buku', 'left'),
  ),
  'where' => array('tphb.transaksi_peminjaman' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->result_array();
  }

  return $result;
 }

}
