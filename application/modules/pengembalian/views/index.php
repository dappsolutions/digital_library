<script src="<?php echo base_url() ?>files/js/controllers/pengembalian.js"></script> 
<div id="content-header">
 <div id="breadcrumb"> <a href="<?php echo base_url() . 'home' ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
  <a href="<?php echo base_url() . 'pengembalian' ?>" class="current">Pengembalian</a> </div>
 <h1>Pengembalian Buku</h1>
</div>
<div class="container-fluid">
 <div class="row-fluid">
  <div class="" style="">
   <div class="span5">
    <div class="widget-box">
     <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5>Form Pengembalian</h5>
     </div>
     <div class="widget-content nopadding">
      <form action="#" method="get" class="form-horizontal">
       <div class="control-group">
        <label class="control-label">NIS</label>
        <div class="controls">
         <input type="text" id="nis" error="NIS" class="span11 required" placeholder="NIS" onkeypress="pengembalian.getAutocompleteSiswaPinjam(this, event)"/>
        </div>
       </div>
       <div class="list_buku">

       </div>       
       <div class="form-actions">
        <button type="button" class="btn btn-success" onclick="pengembalian.save()">Proses</button>
       </div>
      </form>
     </div>
    </div>
   </div>


   <div class="span7">
    <div class="widget-box">
     <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5>Daftar Siswa Pengembalian Buku</h5>
     </div>
     <div class="widget-content nopadding">
      <div class="" style="padding: 16px;">
       <div class="control-group">
        <label class="control-label">NIS</label>
        <div class="controls">
         <input type="text" class="span11 " placeholder="Pencarian" style="width: 70%;" onkeypress="pengembalian.search(this, event)"/>
        </div>
       </div>
       <div class="control-group">
        <!--<label class="control-label">Tanggal Pinjam</label>-->
        <div class="controls" style="max-height: 400px;overflow: auto;">
         <div class=""  id="data_siswa_pinjam">
          <table class="table table-bordered data-table">
           <thead>
            <tr>
             <th>No</th>
             <th>Nis</th>
             <th>Siswa</th>
             <th>Tanggal Pinjam</th>
             <th>Tanggal Pengembalian</th>
             <th>Action</th>
            </tr>
           </thead>
           <tbody>
            <?php if (!empty($data)) { ?>
             <?php $no = 1; ?>
             <?php foreach ($data as $value) { ?>
              <?php
              if ($value['td_id'] != '' &&
              $value['tdbh_id'] != '') {
               $color = 'color:red';
               if ($value['tgl_bayar_denda_pinjam'] != '' &&
               $value['tgl_bayar_denda_buku_hilang'] != '') {
                $color = 'color:green';
               }
              } else {
               if ($value['td_id'] != '' ||
               $value['tdbh_id'] != '') {
                $color = 'color:red';
                if ($value['tgl_bayar_denda_pinjam'] != '' ||
                $value['tgl_bayar_denda_buku_hilang'] != '') {
                 $color = 'color:green';
                }
               }else{
                $color = 'none';
               }
              }
              ?>
              <tr style="<?php echo $color ?>">
               <td><?php echo $no++ ?></td>
               <td><?php echo $value['nis'] ?></td>
               <td><?php echo $value['siswa'] ?></td>
               <td><?php echo date('d M Y', strtotime($value['tanggal_pinjam'])) ?></td>
               <td><?php echo date('d M Y', strtotime($value['tanggal_kembali'])) ?></td>
               <td><i class="mdi mdi-pencil-circle mdi-24px" onclick="pengembalian.detailPeminjaman('<?php echo $value['id'] ?>', '<?php echo $color ?>')"></i></td>
              </tr>
             <?php } ?>
            <?php } else { ?>
             <tr>
              <td colspan="5">Tidak Ada Pengembalian</td>
             </tr>
            <?php } ?>
           </tbody>
          </table>
         </div>         
        </div>
       </div>
      </div>             
     </div>
    </div>
    <br/>
    <hr/>
   </div>
  </div>
 </div><hr>
</div>