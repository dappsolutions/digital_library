<table class="table table-bordered data-table">
 <thead>
  <tr>
   <th>No</th>
   <th>Nis</th>
   <th>Siswa</th>
   <th>Tanggal Pinjam</th>
   <th>Tanggal Pengembalian</th>
   <th>Action</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($data)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($data as $value) { ?>
    <?php
    if ($value['td_id'] != '' &&
    $value['tdbh_id'] != '') {
     $color = 'color:red';
     if ($value['tgl_bayar_denda_pinjam'] != '' &&
     $value['tgl_bayar_denda_buku_hilang'] != '') {
      $color = 'color:green';
     }
    } else {
     if ($value['td_id'] != '' ||
     $value['tdbh_id'] != '') {
      $color = 'color:red';
      if ($value['tgl_bayar_denda_pinjam'] != '' ||
      $value['tgl_bayar_denda_buku_hilang'] != '') {
       $color = 'color:green';
      }
     } else {
      $color = 'none';
     }
    }
    ?>
    <tr style="<?php echo $color ?>">
     <td><?php echo $no++ ?></td>
     <td><?php echo $value['nis'] ?></td>
     <td><?php echo $value['siswa'] ?></td>
     <td><?php echo date('d M Y', strtotime($value['tanggal_pinjam'])) ?></td>
     <td><?php echo date('d M Y', strtotime($value['tanggal_kembali'])) ?></td>
     <td><i class="mdi mdi-pencil-circle mdi-24px" onclick="pengembalian.detailPeminjaman('<?php echo $value['id'] ?>', '<?php echo $color ?>')"></i></td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td colspan="5">Tidak Ada Pengembalian</td>
   </tr>
  <?php } ?>
 </tbody>
</table>