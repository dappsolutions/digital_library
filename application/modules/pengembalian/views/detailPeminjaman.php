<div class="container-fluid">
 <div class="row-fluid">
  <div class="" style="">
   <div class="span12">
    <div class="widget-box">
     <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5>Detail Pengembalian</h5>
     </div>
     <div class="widget-content nopadding">
      <form action="#" method="get" class="form-horizontal">
       <div class="control-group">
        <label class="control-label">NIS</label>
        <div class="controls">
         <input type="text" id="nis" error="NIS" class="span11 required" placeholder="NIS" disabled value="<?php echo $nis ?>"/>
        </div>
       </div>
       <div class="control-group">
        <label class="control-label">Siswa</label>
        <div class="controls">
         <input type="text" id="nis" error="NIS" class="span11 required" placeholder="NIS" disabled value="<?php echo $siswa ?>"/>
        </div>
       </div>
       <div class="control-group">
        <label class="control-label">Tanggal Pinjam</label>
        <div class="controls">
         <input type="text" id="tanggal" error="Tanggal" class="span11 required buku" placeholder="Tanggal" 
                style="width: 72%;" disabled="" value="<?php echo date('d F Y', strtotime($tanggal_pinjam)) ?>"/>
        </div>
       </div>
       
       <div class="control-group">
        <label class="control-label">Tanggal Pengembalian</label>
        <div class="controls">
         <input type="text" id="tanggal" error="Tanggal" class="span11 required buku" placeholder="Tanggal" 
                style="width: 72%;" disabled="" value="<?php echo date('d F Y', strtotime($tanggal_kembali)) ?>"/>
        </div>
       </div>
       
       <div class="control-group">
        <label class="control-label">Total Denda Pengembalian (Rp)</label>
        <div class="controls">
         <input type="text" id="tanggal" error="Tanggal" class="span11 required buku" placeholder="Tanggal" 
                style="width: 72%;" disabled="" value="<?php echo $denda_peminjaman == '' ? 0 : number_format($denda_peminjaman, 2, ',', '.'); ?>"/>
        </div>
       </div>
       
       <div class="control-group">
        <label class="control-label">Total Denda Buku Hilang (Rp)</label>
        <div class="controls">
         <input type="text" id="tanggal" error="Tanggal" class="span11 required buku" placeholder="Tanggal" 
                style="width: 72%;" disabled="" value="<?php echo $denda_buku_hilang == '' ? 0 : number_format($denda_buku_hilang, 2, ',', '.'); ?>"/>
        </div>
       </div>
       <div class="control-group">
        <label class="control-label">Buku Dipinjam</label>
        <?php foreach ($list_buku as $value) {?>
        <div class="controls">
         <input type="text" id="buku" error="Buku" class="span11 required buku" placeholder="Buku" style="width: 72%;" 
                disabled="" value="<?php echo $value['buku'] ?>"/>
         &nbsp;         
          <?php echo $value['buku_hilang'] != '' ? $is_hilang != 'none' ? '<label style="color:red;">Hilang</label>' : '' : '' ?>
        </div>
        <?php } ?>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>