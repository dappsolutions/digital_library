<?php

class Pagination extends MX_Controller {

 public function setPaging($jumlah_data, $limit = 200, $segmen = 3, $module_name) {
  $segmen_pagination = intval($jumlah_data / $limit);
  $data['segmen'] = $segmen_pagination;
  $data['total_segmen'] = $segmen;
  $data['module'] = $module_name;
  $data['limit'] = $limit;
  $view_pagination = $this->load->view('pagination_view', $data, true);
  if (($jumlah_data == $limit) || $segmen == 1) {
   $view_pagination = '';
  }
  return $view_pagination;
 }

 public function get_pagination($url, $uri, $total_rows, $limit, $last_no) {
  $this->load->library('pagination');
  $config["base_url"] = base_url() . $url;
  $config['first_url'] = '1';
  $config['uri_segment'] = $uri;
  $config["total_rows"] = $total_rows;
  $config["per_page"] = $limit;
  $config['use_page_numbers'] = true;

  $config['cur_tag_open'] = "<li class='page-item active'>";
  $config['cur_tag_close'] = "</li>";
  $config['prev_tag_open'] = "<li class='page-item'>";
  $config['prev_tag_close'] = "</li>";
  $config['num_tag_open'] = "<li class='page-item'>";
  $config['num_tag_close'] = "</li>";
  $config['next_tag_open'] = "<li class='page-item'>";
  $config['next_tag_close'] = "</li>";
  $config['first_tag_open'] = "<li class='page-item'>";
  $config['first_tag_close'] = "</li>";
  $config['last_tag_open'] = "<li class='page-item'>";
  $config['last_tag_close'] = "</li>";
  $config['full_tag_open'] = '<div class="col-md-12 text-right"><nav aria-label="Page navigation example"><ul class="pagination justify-content-end">';
  $config['full_tag_close'] = '</ul></nav></div>';
  $this->pagination->initialize($config);
  return array(
      'links' => $this->pagination->create_links(),
      'last_no' => $last_no,
      'total_rows' => $total_rows
  );
 }

 public function get_limit() {
  return 1;
 }

 public function get_custom_limit($limit = 10) {
  return $limit;
 }

}
