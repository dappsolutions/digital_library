<div class="container-fluid">
 <div class="row-fluid">
  <div class="span12">
   <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
     <h5>Edit Siswa</h5>
    </div>
    <div class="widget-content nopadding">
     <form action="#" method="get" class="form-horizontal">
      <div class="control-group">
       <label class="control-label">Nama</label>
       <div class="controls">
        <input type="hidden" id="id" class="" value="<?php echo $id ?>"/>
        <input type="text" id="nama" error="Nama" class="span11 required" placeholder="Nama" value='<?php echo $nama ?>'/>
       </div>
      </div>
      <div class="control-group">
       <label class="control-label">Nis</label>
       <div class="controls">
        <input type="text" class="span11 required" error="Nis" id="nis" placeholder="Nis" value='<?php echo $nis ?>'/>
       </div>
      </div>
      <div class="form-actions">
       <button type="submit" class="btn btn-success" onclick="siswa.exeUpdate()">Save</button>
      </div>
     </form>
    </div>
   </div>
  </div>
 </div><hr>
</div>