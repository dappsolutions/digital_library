<table class="table table-bordered data-table">
 <thead>
  <tr>
   <th>No</th>
   <th>Nama</th>
   <th>NIS</th>
   <th>Action</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($data)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($data as $value) { ?>
    <tr class="text-center">
     <td class="center"><?php echo $no++ ?></td>
     <td class="center"><?php echo $value['nama'] ?></td>
     <td class="center"><?php echo $value['nis'] ?></td>
     <td class="center">
      <i class="mdi mdi-pencil-circle mdi-24px" onclick="siswa.edit('<?php echo $value['id'] ?>')"></i>
      <i class="mdi mdi-delete mdi-24px" onclick="siswa.konfirmasi_delete('<?php echo $value['id'] ?>', '<?php echo $value['nis'] ?>')"></i>
      <i class="mdi mdi-barcode mdi-24px" onclick="siswa.showBarcode('<?php echo $value['nis'] ?>')"></i>
     </td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr class="text-center">
    <td class="center" colspan="4">Tidak Ada Data</td>
   </tr>
  <?php } ?>
 </tbody>
</table>