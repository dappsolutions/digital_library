<script src="<?php echo base_url() ?>files/js/controllers/siswa.js"></script> 
<div id="content-header">
 <div id="breadcrumb"> <a href="<?php echo base_url().'home' ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
  <a href="<?php echo base_url() . 'siswa' ?>" class="current">Siswa</a> </div>
 <h1>Daftar Siswa</h1>
</div>
<div class="container-fluid">
 <div class="row-fluid">
  <div class="span12">
   <div class="row-fluid">
    <div class="span6">
     <button class="btn btn-success" id="" onclick="siswa.add()">Tambah</button>
    </div>
    <div class="span6 text-right">
     <div class="control-group">
      <div class="controls">
       <input class="span6" placeholder="Pencarian Data" type="text" id="" onkeypress="siswa.search(this, event)">
      </div>
     </div>
    </div>
   </div>
   <div class="widget-box">
    <div class="widget-title">
     <span class="icon"><i class="icon-th"></i></span> 
     <h5>Daftar Siswa</h5>
    </div>
    <div class="widget-content nopadding" style="overflow: auto;max-height: 400px;" id="data_siswa">
     <table class="table table-bordered data-table">
      <thead>
       <tr>
        <th>No</th>
        <th>Nama</th>
        <th>NIS</th>
        <th>Action</th>
       </tr>
      </thead>
      <tbody>
       <?php if (!empty($data)) { ?>
        <?php $no = 1; ?>
        <?php foreach ($data as $value) { ?>
         <tr class="text-center">
          <td class="center"><?php echo $no++ ?></td>
          <td class="center"><?php echo $value['nama'] ?></td>
          <td class="center"><?php echo $value['nis'] ?></td>
          <td class="center">
           <i class="mdi mdi-pencil-circle mdi-24px" onclick="siswa.edit('<?php echo $value['id'] ?>')"></i>
           <i class="mdi mdi-delete mdi-24px" onclick="siswa.konfirmasi_delete('<?php echo $value['id'] ?>', '<?php echo $value['nis'] ?>')"></i>
           <i class="mdi mdi-barcode mdi-24px" onclick="siswa.showBarcode('<?php echo $value['nis'] ?>')"></i>           
          </td>
         </tr>
        <?php } ?>
       <?php } else { ?>
        <tr class="text-center">
         <td class="center" colspan="4">Tidak Ada Data</td>
        </tr>
       <?php } ?>
      </tbody>
     </table>
    </div>
   </div>

  </div>
 </div>
</div>