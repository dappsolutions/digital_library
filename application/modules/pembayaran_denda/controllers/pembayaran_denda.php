<?php

class Pembayaran_denda extends MX_Controller {

 public function __construct() {
  parent::__construct();
  if ($this->session->userdata('user') == "") {
   redirect(base_url());
  }
 }

 public function get_module() {
  return "pembayaran_denda";
 }

 public function get_table_name() {
  return 'transaksi_peminjaman';
 }
 
 public function index() {
  $data['module'] = $this->get_module();
  $data['view'] = 'index';
  $data['data'] = $this->getDataPembayaranDenda();
  echo Modules::run('template', $data);
 }

 public function getDataPembayaranDenda() {
  $data = Modules::run('database/get', array(
    'table' => $this->get_table_name().' tp',
    'field'=> array(
     'tp.id as transaksi_id',
     'td.id as transaksi_denda',
     'tdbh.id as transaksi_denda_buku_hilang',
     's.nis as nis',
     's.nama as siswa',
     'td.denda as denda_hari',
     'tdbh.denda as denda_buku',
     'td.tanggal_bayar as tanggal_bayar_denda',
     'tdbh.tanggal_bayar as tanggal_bayar_denda_buku_hilang'
    ),
    'join' => array(
     array('transaksi_denda td', 'tp.id = td.transaksi_peminjaman', 'left'),
     array('transaksi_denda_buku_hilang tdbh', 'tp.id = tdbh.transaksi_peminjaman', 'left'),
     array('siswa s', 'tp.siswa = s.id'),
    ),
    'where'=> "td.denda is not null or tdbh.denda is not null"
  ));
  
  $result = array();
  if(!empty($data)){
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  return $result;
 }
 
 
 public function getDataSearchPembayaranDenda($keyword) {
  $data = Modules::run('database/get', array(
    'table' => $this->get_table_name().' tp',
    'field'=> array(
     'tp.id as transaksi_id',
     'td.id as transaksi_denda',
     'tdbh.id as transaksi_denda_buku_hilang',
     's.nis as nis',
     's.nama as siswa',
     'td.denda as denda_hari',
     'tdbh.denda as denda_buku',
     'td.tanggal_bayar as tanggal_bayar_denda',
     'tdbh.tanggal_bayar as tanggal_bayar_denda_buku_hilang'
    ),
    'join' => array(
     array('transaksi_denda td', 'tp.id = td.transaksi_peminjaman', 'left'),
     array('transaksi_denda_buku_hilang tdbh', 'tp.id = tdbh.transaksi_peminjaman', 'left'),
     array('siswa s', 'tp.siswa = s.id'),
    ),
    'where'=> "td.denda is not null or tdbh.denda is not null",
    'inside_brackets'=> true,
    'is_or_like'=> true,
    'like'=> array(
     array('tp.id', $keyword),
     array('td.id', $keyword),
     array('tdbh.id', $keyword),
     array('s.nis', $keyword),
     array('s.nama', $keyword),
     array('td.denda', $keyword),
     array('tdbh.denda', $keyword),
     array('td.tanggal_bayar', $keyword),
     array('tdbh.tanggal_bayar', $keyword)
    )
  ));
  
  $result = array();
  if(!empty($data)){
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  return $result;
 }
 
 public function search() {
  $keyword = rawurldecode($this->input->post('keyword'));
  $data = $this->getDataSearchPembayaranDenda($keyword);
  
  $content['data'] = $data;
  echo $this->load->view('search', $content, true);
 }
 
 public function bayar_denda_buku_hilang($transaksi_denda_buku_hilang) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Tambah Pembayaran Denda Buku Hilang');
   Modules::run('database/_update', 'transaksi_denda_buku_hilang', array('tanggal_bayar'=> date('Y-m-d')), 
    array('id'=> $transaksi_denda_buku_hilang));
   Modules::run('helper/_insert_change_log', 'transaksi_denda_buku_hilang', $transaksi_denda_buku_hilang, $event);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  
  echo json_encode(array('is_valid'=> $is_valid));
 }
 
 public function bayar_denda($transaksi_denda) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Tambah Pembayaran Denda Buku Pinjam');
   Modules::run('database/_update', 'transaksi_denda', array('tanggal_bayar'=> date('Y-m-d')), 
    array('id'=> $transaksi_denda));
   Modules::run('helper/_insert_change_log', 'transaksi_denda', $transaksi_denda, $event);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  
  echo json_encode(array('is_valid'=> $is_valid));
 }
}
