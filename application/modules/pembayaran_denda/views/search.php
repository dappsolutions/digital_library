<table class="table table-bordered data-table">
 <thead>
  <tr>
   <th>No</th>
   <th>Kode Transaksi Peminjaman</th>
   <th>NIS</th>
   <th>Nama</th>
   <th>Denda Pinjam</th>
   <th>Denda Buku Hilang</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($data)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($data as $value) { ?>
    <tr class="text-center">
     <td class="center"><?php echo $no++ ?></td>
     <td class="center"><?php echo $value['transaksi_id'] ?></td>
     <td class="center"><?php echo $value['nis'] ?></td>
     <td class="center"><?php echo $value['siswa'] ?></td>
     <td class="center"><?php echo $value['denda_hari'] == '' ? 'Tidak Ada' : number_format($value['denda_hari'], 2, ',', '.') ?>
      &nbsp;
      <?php if ($value['transaksi_denda'] != '' && $value['tanggal_bayar_denda'] == '') { ?>
       <i class="mdi mdi-check-circle mdi-24px" 
          data-container="body" onmouseover="pembayaran_denda.show_tooltip(this)" data-toggle="tooltip" 
          title="Bayar Denda Buku Peminjaman"
          onclick="pembayaran_denda.bayar_denda('<?php echo $value['transaksi_denda'] ?>')"></i>
         <?php } ?>

      <?php if ($value['transaksi_denda'] != '' && $value['tanggal_bayar_denda'] != '') { ?>
       <?php echo '<label style="color:green">Tanggal Bayar (' . date('d M Y', strtotime($value['tanggal_bayar_denda'])) . ')</label>' ?>
      <?php } ?>
     </td>
     <td class="center"><?php echo $value['denda_buku'] == '' ? 'Tidak Ada' : number_format($value['denda_buku'], 2, ',', '.') ?>
      &nbsp;
      <?php if ($value['transaksi_denda_buku_hilang'] != '' && $value['tanggal_bayar_denda_buku_hilang'] == '') { ?>
       <i class="mdi mdi-check-circle mdi-24px" 
          data-container="body" onmouseover="pembayaran_denda.show_tooltip(this)" data-toggle="tooltip" 
          title="Bayar Denda Buku Hilang"
          onclick="pembayaran_denda.bayar_denda_buku_hilang('<?php echo $value['transaksi_denda_buku_hilang'] ?>')"></i>
         <?php } ?>

      <?php if ($value['transaksi_denda_buku_hilang'] != '' && $value['tanggal_bayar_denda_buku_hilang'] != '') { ?>
       <?php echo '<label style="color:green">Tanggal Bayar (' . date('d M Y', strtotime($value['tanggal_bayar_denda_buku_hilang'])) . ')</label>' ?>
      <?php } ?>
     </td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr class="text-center">
    <td class="center" colspan="3">Tidak Ada Data</td>
   </tr>
  <?php } ?>
 </tbody>
</table>