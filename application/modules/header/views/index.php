<head>
 <title>Digital Library</title>
 <meta charset="UTF-8" />
 <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 <link rel="stylesheet" href="<?php echo base_url() ?>files/css/bootstrap.min.css" />
 <link rel="stylesheet" href="<?php echo base_url() ?>files/css/bootstrap-responsive.min.css" />
 <link rel="stylesheet" href="<?php echo base_url() ?>files/css/fullcalendar.css" />
 <link rel="stylesheet" href="<?php echo base_url() ?>files/css/maruti-style.css" />
 <link rel="stylesheet" href="<?php echo base_url() ?>files/css/maruti-media.css" class="skin-color" />
 <link rel="stylesheet" href="<?php echo base_url() ?>files/css/toastr.min.css" />
 <link rel="stylesheet" href="<?php echo base_url() ?>files/mdi/css/materialdesignicons.min.css" />

 
 <script src="<?php echo base_url() ?>files/js/jquery.js"></script> 
 <script src="<?php echo base_url() ?>files/js/jquery.min.js"></script>  
 <script src="<?php echo base_url() ?>files/js/fusioncharts.js"></script> 
 <script src="<?php echo base_url() ?>files/js/fusioncharts.charts.js"></script> 
 <script src="<?php echo base_url() ?>files/js/controllers/dashboard.js"></script>  
 <script src="<?php echo base_url() ?>files/js/bootbox.js"></script>  
 <script src="<?php echo base_url() ?>files/js/excanvas.min.js"></script>  
 <script src="<?php echo base_url() ?>files/js/jquery.ui.custom.js"></script> 
 <script src="<?php echo base_url() ?>files/js/bootstrap.min.js"></script> 
 <script src="<?php echo base_url() ?>files/js/jquery.flot.min.js"></script> 
 <script src="<?php echo base_url() ?>files/js/jquery.flot.resize.min.js"></script> 
 <script src="<?php echo base_url() ?>files/js/jquery.peity.min.js"></script> 
 <script src="<?php echo base_url() ?>files/js/fullcalendar.min.js"></script> 
 <script src="<?php echo base_url() ?>files/js/maruti.js"></script> 
 <script src="<?php echo base_url() ?>files/js/maruti.dashboard.js"></script> 
 <script src="<?php echo base_url() ?>files/js/maruti.chat.js"></script> 
 <script src="<?php echo base_url() ?>files/js/toastr.min.js"></script>  
 <script src="<?php echo base_url() ?>files/js/url.js"></script>  
 <script src="<?php echo base_url() ?>files/js/pagination.js"></script>
 <script src="<?php echo base_url() ?>files/js/JsBarcode.all.min.js"></script>  


 <script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage(newURL) {

   // if url is empty, skip the menu dividers and reset the menu selection to default
   if (newURL != "") {

    // if url is "-", it is this page -- reset the menu:
    if (newURL == "-") {
     resetMenu();
    }
    // else, send page to designated URL            
    else {
     document.location.href = newURL;
    }
   }
  }

  // resets the menu selection upon entry to this page:
  function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
  }
 </script>
</head>