<html>
 <head>
  <title>title</title>
 </head>
 <body>

  <div class="">
   <table class="">
    <tbody>
     <tr>
      <td style="font-weight: bold;">PERPUSTAKAN SMKN 2 BLITAR
       <br/>Jl. Tanjung<br/>
       ===========================
      </td>
      <?php for ($i = 0; $i < 40; $i++) { ?>
       <td>
        &nbsp;
       </td>
      <?php } ?>
      <td>
       <table class="">
        <tbody>
         <tr>
          <td style="font-weight: bold;">Nama</td>
          <td>:</td>
          <td><?php echo $siswa ?></td>
         </tr>
         <tr>
          <td style="font-weight: bold;">Nis</td>
          <td>:</td>
          <td><?php echo $nis ?></td>
         </tr>
         <tr>
          <td style="font-weight: bold;">Kelas</td>
          <td>:</td>
          <td>.............</td>
         </tr>
        </tbody>
       </table>
      </td>
     </tr>
    </tbody>
   </table>

  </div>

  <div style="text-align: center;font-size: 12px;">
   <h4>DAFTAR PEMINJAMAN BUKU PAKET PERORANGAN</h4>
   <hr/>
   
   <table class="" style="width: 100%;border-collapse: collapse;font-size: 12px;">
    <thead>
     <tr>
      <th style="border:1px solid #333;">NO</th>
      <th style="border:1px solid #333;">JUDUL BUKU</th>
      <th style="border:1px solid #333;">KODE BUKU</th>
      <th style="border:1px solid #333;">TANGGAL PINJAM</th>
      <th style="border:1px solid #333;">TANGGAL KEMBALI</th>
     </tr>
    </thead>
    <tbody>
     <?php 
     $no =1;
     foreach ($list_buku as $value) {?>
     <tr>
      <td style="text-align: center;border:1px solid #333;"><?php echo $no++ ?></td>
      <td style="text-align: center;border:1px solid #333;"><?php echo $value['buku'] ?></td>
      <td style="text-align: center;border:1px solid #333;"><?php echo $value['kode_buku'] ?></td>
      <td style="text-align: center;border:1px solid #333;"><?php echo date('d M Y', strtotime($tanggal_pinjam)); ?></td>
      <td style="text-align: center;border:1px solid #333;"><?php echo $tanggal_kembali ?></td>
     </tr>
     <?php } ?>
    </tbody>
   </table>      
  </div>   
  
  <div class="">
   <p style="font-weight: bold;font-size: 12px;">Dengan Janji</p>
   <ul style="font-weight: bold;font-size: 12px;">
    <li>Bahwa saya sanggup mentaati/merawat menjaga keutuhan da kebersihan dari buku yang saya pinjam dengan sebaik - baiknya.</li>
    <li>Janji saya buat dengan tidak ada masalah atau paksaan dari pihak manapun.</li>
    <li>Apabila saya melanggar janji, saya sanggup diberi sanksi sesuai peraturan yang berlaku di Perpustakaan SMKN 2 Blitar.</li>
   </ul>
  </div>
  
  <br/>
  <div class="">
   <table class="">
    <tbody>
     <tr>
      <td style="font-size: 12px;">Pengelola Perpustakaan
      <br/>
      <br/>
      <br/>
      <br/>
      ...............................
      </td>
      <?php for($i=0; $i< 50; $i++){ ?>
      <td>&nbsp;</td>
      <?php } ?>
      <td style="font-size: 12px;">Blitar, .....................
      <br/>
      Peminjam, <br/><br/><br/>
      ...................................
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </body>
</html>
