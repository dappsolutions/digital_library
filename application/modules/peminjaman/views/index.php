<script src="<?php echo base_url() ?>files/js/controllers/peminjaman.js"></script> 
<div id="content-header">
 <div id="breadcrumb"> <a href="<?php echo base_url() . 'home' ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> 
  <a href="<?php echo base_url() . 'peminjaman' ?>" class="current">Peminjaman</a> </div>
 <h1>Peminjaman Buku</h1>
</div>
<div class="container-fluid">
 <div class="row-fluid">
  <div class="" style="">
   <div class="span5">
    <div class="widget-box">
     <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5>Form Peminjaman</h5>
     </div>
     <div class="widget-content nopadding">
      <form action="#" method="get" class="form-horizontal">
       <div class="control-group">
        <label class="control-label">NIS</label>
        <div class="controls">
         <input type="text" id="nis" error="NIS" class="span11 required" placeholder="NIS" onkeypress="peminjaman.getAutocompleteSiswa(this)"/>
        </div>
       </div>
       <div class="control-group">
        <label class="control-label">Buku</label>
        <div class="controls">
         <input type="text" id="buku" error="Buku" class="span11 required buku" placeholder="No Induk Buku / Buku / Scan Barcode Buku" style="width: 72%;" 
                onkeypress="peminjaman.getAutocompleteBuku(this, event)" 
                no_induk='' id_buku=''/>
         &nbsp;&nbsp;
         <i class="mdi mdi-plus-circle mdi-24px" onclick="peminjaman.addBuku(this)"></i>
         <i class="mdi mdi-minus-circle mdi-24px" onclick="peminjaman.removeBuku(this)"></i>
        </div>
       </div>
       <div class="form-actions">
        <button type="button" class="btn btn-success" onclick="peminjaman.save()">Proses</button>
       </div>
      </form>
     </div>
    </div>
   </div>


   <div class="span7">
    <div class="widget-box">
     <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
      <h5>Daftar Siswa Peminjam Buku</h5>
     </div>
     <div class="widget-content nopadding">
      <div class="" style="padding: 16px;">
       <div class="control-group">
        <label class="control-label">NIS</label>
        <div class="controls">
         <input type="text" class="span11 " placeholder="Pencarian" style="width: 70%;" onkeypress="peminjaman.search(this, event)"/>
        </div>
       </div>
       <div class="control-group">
        <!--<label class="control-label">Tanggal Pinjam</label>-->
        <div class="controls" style="max-height: 400px;overflow: auto;">
         <div class=""  id="data_siswa_pinjam">
          <table class="table table-bordered data-table">
          <thead>
           <tr>
            <th>No</th>
            <th>Nis</th>
            <th>Siswa</th>
            <th>Tanggal Pinjam</th>
            <th>Tanggal Kembali</th>
            <th>Action</th>
           </tr>
          </thead>
          <tbody>
           <?php if (!empty($data)) { ?>
            <?php $no = 1; ?>
            <?php foreach ($data as $value) { ?>
             <tr>
              <td><?php echo $no++ ?></td>
              <td><?php echo $value['nis'] ?></td>
              <td><?php echo $value['siswa'] ?></td>
              <td><?php echo date('d M Y', strtotime($value['tanggal_pinjam'])) ?></td>
              <td><?php echo date('d M Y', strtotime($value['tanggal_kembali'])) ?></td>
              <td>
               <i class="mdi mdi-pencil-circle mdi-24px" onclick="peminjaman.detailPeminjaman('<?php echo $value['id'] ?>')"></i>&nbsp;
               <i class="mdi mdi-printer mdi-24px" onclick="peminjaman.printPeminjaman('<?php echo $value['id'] ?>')"></i>
              </td>
             </tr>
            <?php } ?>
           <?php }else{ ?>
             <tr>
              <td colspan="5">Tidak Ada Peminjam</td>
             </tr>
           <?php } ?>
          </tbody>
         </table>
         </div>         
        </div>
       </div>
      </div>             
     </div>
    </div>
    <br/>
    <hr/>
   </div>
  </div>
 </div><hr>
</div>