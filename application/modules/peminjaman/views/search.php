
<table class="table table-bordered data-table">
 <thead>
  <tr>
   <th>No</th>
   <th>Nis</th>
   <th>Siswa</th>
   <th>Tanggal Pinjam</th>
   <th>Tanggal Kembali</th>
   <th>Action</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($data)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($data as $value) { ?>
    <tr>
     <td><?php echo $no++ ?></td>
     <td><?php echo $value['nis'] ?></td>
     <td><?php echo $value['siswa'] ?></td>
     <td><?php echo date('d M Y', strtotime($value['tanggal_pinjam'])) ?></td>
     <td><?php echo date('d M Y', strtotime($value['tanggal_kembali'])) ?></td>
     <td>
      <i class="mdi mdi-pencil-circle mdi-24px" onclick="peminjaman.detailPeminjaman('<?php echo $value['id'] ?>')"></i>&nbsp;
      <i class="mdi mdi-printer mdi-24px" onclick="peminjaman.printPeminjaman('<?php echo $value['id'] ?>')"></i>
     </td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr>
    <td colspan="5">Tidak Ada Peminjam</td>
   </tr>
  <?php } ?>
 </tbody>
</table>