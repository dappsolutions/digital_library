<?php

class Peminjaman extends MX_Controller {

 public function __construct() {
  parent::__construct();
  if ($this->session->userdata('user') == "") {
   redirect(base_url());
  }
 }

 public function get_module() {
  return "peminjaman";
 }

 public function get_table_name() {
  return 'transaksi_peminjaman';
 }

 public function index() {
  $data['module'] = $this->get_module();
  $data['data'] = $this->getDatapeminjaman();
  $data['view'] = 'index';
  echo Modules::run('template', $data);
 }

 public function cetakFormulirPeminjaman($id) {
  require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
  $pdf = new \Mpdf\Mpdf();
//  echo '<pre>';
//  print_r(get_class_methods($mpdf));die;
  $data = Modules::run('database/get', array(
              'table' => $this->get_table_name() . ' tp',
              'field' => array('tp.id', 's.nama as siswa', 's.nis', 'tp.tanggal_pinjam', 'rp.jumlah_hari'),
              'join' => array(
                  array('siswa s', 'tp.siswa = s.id'),
                  array('range_peminjaman rp', 'tp.range_peminjaman = rp.id'),
              ),
              'where' => array('tp.id' => $id)
  ));

  if (!empty($data)) {
   $data = $data->row_array();
   $data['tanggal_kembali'] = date('d M Y', strtotime($data['tanggal_pinjam'] . " + " . $data['jumlah_hari'] . ' days '));
  }

  $data['list_buku'] = $this->getListBukuDipinjam($id);
  $view = $this->load->view('cetakFormulirPeminjaman', $data, true);
  $pdf->WriteHTML($view);
  $pdf->Output('formulir_' . $id . '.pdf', 'I');
 }

 public function checkBukuAvailable() {
  $buku = $this->input->post('buku');
  $data = Modules::run('database/get', array(
              'table' => 'buku',
              'where' => "(no_induk_buku = '" . $buku . "' or nama = '" . $buku . "') and stock > 0"
  ));

  $is_valid = false;
  $buku = "";
  $id = '';
  $no_induk_buku = '';
  if (!empty($data)) {
   $data = $data->row_array();
   $buku = $data['nama'];
   $id = $data['id'];
   $no_induk_buku = $data['no_induk_buku'];
   $is_valid = true;
  }

  echo json_encode(array(
      'is_valid' => $is_valid,
      'buku' => $buku,
      'id' => $id,
      'no_induk_buku' => $no_induk_buku
  ));
 }

 public function getDatapeminjaman() {
  $data = Modules::run('database/get', array(
              'table' => $this->get_table_name() . ' tp',
              'field' => array('tp.id', 's.nama as siswa', 's.nis', 'tp.tanggal_pinjam',
                  'rp.jumlah_hari'),
              'join' => array(
                  array('siswa s', 'tp.siswa = s.id'),
                  array('range_peminjaman rp', 'tp.range_peminjaman = rp.id'),
              ),
              'where' => 'tp.tanggal_kembali is null'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tanggal_kembali'] = date('d M Y', strtotime($value['tanggal_pinjam'] . " + " . $value['jumlah_hari'] . ' days '));
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function search() {
  $keyword = rawurldecode($this->input->post('keyword'));
  $data = Modules::run('database/get', array(
              'table' => $this->get_table_name() . ' tp',
              'field' => array('tp.id', 's.nama as siswa', 's.nis', 'tp.tanggal_pinjam', 'rp.jumlah_hari'),
              'join' => array(
                  array('siswa s', 'tp.siswa = s.id'),
                  array('range_peminjaman rp', 'tp.range_peminjaman = rp.id')
              ),
              'where' => 'tp.tanggal_kembali is null',
              'inside_brackets' => true,
              'is_or_like' => true,
              'like' => array(
                  array('tp.id', $keyword),
                  array('s.nama', $keyword),
                  array('s.nis', $keyword),
                  array('tp.tanggal_pinjam', $keyword),
              )
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['tanggal_kembali'] = date('d M Y', strtotime($value['tanggal_pinjam'] . " + " . $value['jumlah_hari'] . ' days '));
    array_push($result, $value);
   }
  }


  $content['data'] = $result;
  echo $this->load->view('search', $content, true);
 }

 public function add() {
  echo $this->load->view('add', array(), true);
 }

 public function getAutocompleteSiswa() {
  $keyword = $this->input->get('term');

  $data = Modules::run('database/get', array(
              'table' => 'siswa s',
              'field' => array('s.nama', 's.nis', 's.id'),
              'like' => array(
                  array('s.nis', $keyword)
              )
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result() as $row) {
    if (!$this->siswaHasPeminjaman($row->id)) {
     $result[] = array(
         'label' => $row->nama,
         'value' => $row->nis,
         'id' => $row->id
     );
    }
   }
  }
  echo json_encode($result);
 }

 public function siswaHasPeminjaman($siswa) {
  $data = Modules::run('database/get', array(
              'table' => $this->get_table_name(),
              'where' => array('siswa' => $siswa, 'tanggal_kembali' => null),
              'orderby' => 'id desc'
  ));

  $has_peminjaman = false;
  if (!empty($data)) {
   $has_peminjaman = true;
  }

  return $has_peminjaman;
 }

 public function getAutocompleteBuku() {
  $keyword = $this->input->get('term');

  $data = Modules::run('database/get', array(
              'table' => 'buku buk',
              'field' => array('buk.nama', 'buk.id', 'buk.no_induk_buku'),
              'inside_brackets' => true,
              'like' => array(
                  array('buk.nama', $keyword),
                  array('buk.no_induk_buku', $keyword),
              ),
              'where' => "buk.stock > 0",
              'limit' => 100
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result() as $row) {
    $result[] = array(
        'label' => $row->no_induk_buku . ' - ' . $row->nama,
        'value' => $row->no_induk_buku . ' - ' . $row->nama,
        'id' => $row->id,
        'no_induk_buku' => $row->no_induk_buku
    );
   }
  }
  echo json_encode($result);
 }

 public function get_post_data($value) {
  $data['peminjaman'] = $value->peminjaman;

  return $data;
 }

 public function getIdSiswa($nis) {
  $data = Modules::run('database/get', array(
              'table' => 'siswa',
              'where' => array('nis' => $nis)
  ));

  $id = '';
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  }

  return $id;
 }

 public function getIdBuku($buku) {
  $data = Modules::run('database/get', array(
              'table' => 'buku',
              'where' => array('nama' => $buku)
  ));

  $id = '';
  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  }

  return $id;
 }

 public function getIdRangePeminjaman() {
  $data = Modules::run('database/get', array(
              'table' => 'range_peminjaman',
              'where' => "period_end is null"
          ))->row_array();

  return $data['id'];
 }

 public function get_post_data_transaksi($value) {
  $data['siswa'] = $this->getIdSiswa($value->nis);
  $data['range_peminjaman'] = $this->getIdRangePeminjaman();
  $data['tanggal_pinjam'] = date('Y-m-d');

  return $data;
 }

 public function get_post_data_transaksi_has_buku($value, $transaksi_peminjaman) {
  $data['buku'] = $value->id;
  $data['transaksi_peminjaman'] = $transaksi_peminjaman;

  return $data;
 }

 public function save() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Tambah peminjaman');
   $data_transaksi = $this->get_post_data_transaksi($data);
   //update buku to out      
   $transaksi_peminjaman = Modules::run('database/_insert', $this->get_table_name(), $data_transaksi);
   Modules::run('helper/_insert_change_log', $this->get_table_name(), $transaksi_peminjaman, $event);
   foreach ($data->buku as $value) {
    $data_transaksi_has_buku = $this->get_post_data_transaksi_has_buku($value, $transaksi_peminjaman);
    $transaksi_peminjaman_has_buku = Modules::run('database/_insert', 'transaksi_peminjaman_has_buku', $data_transaksi_has_buku);
    Modules::run('helper/_insert_change_log', 'transaksi_peminjaman_has_buku', $transaksi_peminjaman_has_buku, $event);
    $data_buku = $this->getDataBuku($value->id);
    $buku_keluar = $data_buku['buku_keluar'] == '' ? 0 : $data_buku['buku_keluar'];
    $stok_buku = $data_buku['stock'] - 1;

    $total_buku_keluar = ($buku_keluar + 1);
    Modules::run('database/_update', 'buku',
            array(
                'buku_keluar' => $total_buku_keluar,
                'stock' => $stok_buku
            ),
            array('id' => $data_buku['id']));
    Modules::run('helper/_insert_change_log', 'buku', $data_transaksi_has_buku['buku'], $event);
    $buku_pinjam = Modules::run('database/_insert', 'buku_pinjam', array(
                'buku' => $data_buku['id']
    ));
    Modules::run('helper/_insert_change_log', 'buku_pinjam', $buku_pinjam, $event);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getDataBuku($id) {
  $data = Modules::run('database/get', array(
              'table' => 'buku',
              'where' => array('id' => $id)
  ));

  return $data->row_array();
 }

 public function detailPeminjaman($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->get_table_name() . ' tp',
              'field' => array('tp.tanggal_pinjam', 's.nama as siswa', 's.nis', 'rp.jumlah_hari'),
              'join' => array(
                  array('siswa s', 'tp.siswa = s.id'),
                  array('range_peminjaman rp', 'tp.range_peminjaman = rp.id'),
              ),
              'where' => array('tp.id' => $id)
          ))->row_array();
  $data['tanggal_kembali'] = date('d F Y', strtotime($data['tanggal_pinjam'] . " + " . $data['jumlah_hari'] . ' days '));
  $data['list_buku'] = $this->getListBukuDipinjam($id);
  echo $this->load->view('detailPeminjaman', $data, true);
 }

 public function getListBukuDipinjam($id) {
  $data = Modules::run('database/get', array(
              'table' => 'transaksi_peminjaman_has_buku tphb',
              'field' => array('buk.nama as buku', 'buk.id as kode_buku'),
              'join' => array(
                  array('buku buk', 'tphb.buku = buk.id'),
              ),
              'where' => array('tphb.transaksi_peminjaman' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->result_array();
  }

  return $result;
 }

}
