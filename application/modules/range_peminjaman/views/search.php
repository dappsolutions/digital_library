<table class="table table-bordered data-table">
 <thead>
  <tr>
   <th>No</th>
   <th>Jumlah Hari</th>
   <th>Tanggal Aktif</th>
   <th>Tanggal Expired</th>
   <th>Action</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($data)) { ?>
   <?php $no = 1; ?>
   <?php foreach ($data as $value) { ?>
    <tr class="text-center">
     <td class="center"><?php echo $no++ ?></td>
     <td class="center"><?php echo $value['jumlah_hari'] ?></td>
     <td class="center"><?php echo date('d M Y', strtotime($value['period_start'])) ?></td>
     <td class="center"><?php echo $value['period_end'] != '' ? date('d M Y', strtotime($value['period_end'])) : '' ?></td>
     <td class="center">
      <?php if ($value['period_end'] == '') { ?>
       <i class="mdi mdi-pencil-circle mdi-24px" onclick="range_peminjaman.edit('<?php echo $value['id'] ?>')"></i>
      <?php } ?>
     </td>
    </tr>
   <?php } ?>
  <?php } else { ?>
   <tr class="text-center">
    <td class="center" colspan="3">Tidak Ada Data</td>
   </tr>
  <?php } ?>
 </tbody>
</table>