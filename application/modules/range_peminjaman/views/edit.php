<div class="container-fluid">
 <div class="row-fluid">
  <div class="span12">
   <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
     <h5>Edit Range Peminjaman</h5>
    </div>
    <div class="widget-content nopadding">
     <form action="#" method="get" class="form-horizontal">
      <div class="control-group">
       <label class="control-label">Jumlah Hari</label>
       <div class="controls">
        <input type="hidden" id="id" class="" value="<?php echo $id ?>"/>
        <input type="text" id="jumlah_hari_data" error="Jumlah Hari" class="span11 required" placeholder="Jumlah Hari" value='<?php echo $jumlah_hari ?>'/>
       </div>
      </div>
      <div class="form-actions">
       <button type="submit" class="btn btn-success" onclick="range_peminjaman.exeUpdate()">Save</button>
      </div>
     </form>
    </div>
   </div>
  </div>
 </div><hr>
</div>