<?php

class Range_peminjaman extends MX_Controller {

 public function __construct() {
  parent::__construct();
  if ($this->session->userdata('user') == "") {
   redirect(base_url());
  }
 }

 public function get_module() {
  return "range_peminjaman";
 }

 public function get_table_name() {
  return 'range_peminjaman';
 }
 
 public function index() {
  $data['module'] = $this->get_module();
  $data['view'] = 'index';
  $data['data'] = $this->getDataRangePeminjaman();
  echo Modules::run('template', $data);
 }

 public function getDataRangePeminjaman() {
  $data = Modules::run('database/get', array(
    'table' => $this->get_table_name()
  ));
  
  $result = array();
  if(!empty($data)){
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  return $result;
 }
 
 public function search() {
  $keyword = rawurldecode($this->input->post('keyword'));
  $data = Modules::run('database/get', array(
    'table' => $this->get_table_name(),
    'is_or_like'=> true,
    'like'=> array(
     array('jumlah_hari', $keyword)
    )
  ));
  
  $result = array();
  if(!empty($data)){
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  
  $content['data'] = $result;
  echo $this->load->view('search', $content, true);
 }
 
 public function add() {
  echo $this->load->view('add', array(), true);
 }
 
 public function get_post_data($value) {
  $data['jumlah_hari'] = $value->jumlah_hari;
  $data['period_start'] = date('Y-m-d');
  return $data;
 }
 
 public function execSave() {
  $data = json_decode($this->input->post('data'));
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Tambah Range Peminjaman');
   $data_range_peminjaman = $this->get_post_data($data);
   
   Modules::run('database/_update', $this->get_table_name(), array('period_end'=> date('Y-m-d')), array());
   
   $range_peminjaman = Modules::run('database/_insert', $this->get_table_name(), $data_range_peminjaman);
   Modules::run('helper/_insert_change_log', $this->get_table_name(), $range_peminjaman, $event);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  
  echo json_encode(array('is_valid'=> $is_valid));
 }
 
 public function edit($id) {
  $data = Modules::run('database/get', array(
    'table' => $this->get_table_name(),
    'where' => array('id'=> $id)
  ))->row_array();
  
  echo $this->load->view('edit', $data, true);
 }
 
 public function exeUpdate() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $event = Modules::run('helper/_insert_event', 'Update Range Peminjaman');   
   Modules::run('database/_update', $this->get_table_name(), array('period_end'=> date('Y-m-d')), array('id'=> $id));   
   $data_range_peminjaman = $this->get_post_data($data);
   $range_peminjaman = Modules::run('database/_insert', $this->get_table_name(), $data_range_peminjaman);
   Modules::run('helper/_insert_change_log', $this->get_table_name(), $range_peminjaman, $event);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }
  
  echo json_encode(array('is_valid'=> $is_valid));
 }
}
