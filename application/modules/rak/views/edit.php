<div class="container-fluid">
 <div class="row-fluid">
  <div class="span12">
   <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
     <h5>Edit Rak</h5>
    </div>
    <div class="widget-content nopadding">
     <form action="#" method="get" class="form-horizontal">
      <div class="control-group">
       <label class="control-label">Rak</label>
       <div class="controls">
        <input type="hidden" id="id" class="" value="<?php echo $id ?>"/>
        <input type="text" id="rak_data" error="Rak" class="span11 required" placeholder="Rak" value='<?php echo $rak ?>'/>
       </div>
      </div>
      <div class="form-actions">
       <button type="submit" class="btn btn-success" onclick="rak.exeUpdate()">Save</button>
      </div>
     </form>
    </div>
   </div>
  </div>
 </div><hr>
</div>