<div class="container-fluid">
 <div class="row-fluid">
  <div class="span12">
   <div class="widget-box">
    <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
     <h5>Tambah Rak</h5>
    </div>
    <div class="widget-content nopadding">
     <form action="#" method="get" class="form-horizontal">
      <div class="control-group">
       <label class="control-label">Rak</label>
       <div class="controls">
        <input type="text" id="rak_data" error="Rak" class="span11 required" placeholder="Rak" />
       </div>
      </div>
      <div class="form-actions">
       <button type="submit" class="btn btn-success" onclick="rak.save()">Save</button>
      </div>
     </form>
    </div>
   </div>
  </div>
 </div><hr>
</div>