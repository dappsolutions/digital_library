<?php

class Upload_buku extends MX_Controller {

 public function index() {
  $data['module'] = 'upload_buku';
  $data['view'] = 'index';
  echo Modules::run('template', $data);
 }

 public function getExistIdDataKategori($kategori) {
  $data = Modules::run('database/get', array(
  'table' => 'kategori',
  'where' => array('kategori' => $kategori)
  ));

  if (!empty($data)) {
   $data = $data->row_array();
   $id = $data['id'];
  } else {
   $id = Modules::run('database/_insert', 'kategori', array('kategori' => $kategori));
  }

  return $id;
 }

 public function isExistBukuNoInduk($no_induk_buku) {
  $data = Modules::run('database/get', array(
  'table' => 'buku',
  'where' => array('no_induk_buku' => $no_induk_buku)
  ));

  $is_exist = false;
  if (!empty($data)) {
   $is_exist = true;
  }

  return $is_exist;
 }

 public function upload() {
  $file = fopen('E:/2016-2017.csv', 'r');  
  $data_buku = array();

//  $no = 0;
  try {
   while (!feof($file)) {
    $data = fgetcsv($file);    
    if (trim($data[1]) != '') {
     $data_buku['no_induk_buku'] = $data[1];
     $data_buku['nama'] = $data[3];
     $data_buku['judul'] = $data[3];
     $data_buku['pengarang'] = $data[2];
     $data_buku['keterangan'] = 'Penerbit : ' . $data[5];
     $data_buku['denda'] = $data[9];
     $data_buku['harga'] = $data[10];
     $data_buku['rak'] = 6;
     $data_buku['stock'] = $data[14] == '' ? 0 : $data[14];
     $data_buku['kategori'] = $this->getExistIdDataKategori($data[13]);
     $is_exist = $this->isExistBukuNoInduk(trim($data[1]));
     if (!$is_exist) {
      Modules::run('database/_insert', 'buku', $data_buku);
     }
    }
   }
  } catch (Exception $exc) {
   echo $exc->getTraceAsString();
  }

  fclose($file);
 }

}
